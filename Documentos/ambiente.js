module.exports.getAmbiente = function () {

    /*
        Ambiente 1 Produção,
        Ambiente 2 Homologação,
        Ambiente 3 Localhost,
    */

    var ambiente = 1;

    if(ambiente == 1) {
        return {'ambienteCode' : ambiente, 'ambienteLiteral' : "api.leilao24h.com.br/"};
    } else if(ambiente == 2) {
        return {'ambienteCode' : ambiente, 'ambienteLiteral' : "31.220.57.245/leilao/"};
    } else {
        return {'ambienteCode' : ambiente, 'ambienteLiteral' :"localhost/leilao/"};
    }

}