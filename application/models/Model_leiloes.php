<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_leiloes extends MY_Model {

	public function insert($valores) {
		$this->db->insert('cad_leilao', $valores);
		return $this->db->insert_id();
	}

	public function update($valores) {	
		$this->db->where('id_leilao', $valores['id_leilao']);
		return $this->db->update('cad_leilao', $valores);
	}

	public function view_lista_leiloes() {

		return 	array('leiloes' => 
						$this->db->select("id_leilao,round(valor_minimo,2) as valor_minimo,round(lance_minimo,2) as lance_minimo,ingressos,date_format(data_inicio,'%d/%m/%Y às  %H:%i:%s') as data_inicio,date_format(data_fim_previsto,'%d/%m/%Y às  %H:%i:%s') as data_fim_previsto,status,nome_produto,fk_usuario_arrematou,nome_usuario_arrematou,lance_formatado,nome_usuario_leiloando,nome_categoria, (select count(*) from sala_leilao where fk_leilao = id_leilao) as usuarios")
								->get('view_leilao')
								->result()
				);

	}

	public function listarRecompensas($where = null) {
		
		$this->db->select('id_produto, concat(left(nome_produto,200),\'...\') as nome_produto, concat(left(descricao_produto,300),\'...\') as descricao_produto, nome_categoria, produto_usado, produto_recompensa_custo');
		$this->db->join('cad_categorias', 'cad_produtos.fk_categoria = cad_categorias.id_categoria', 'left');
		$this->db->where('produto_recompensa', 1);
		$this->db->order_by('nome_categoria', 'asc');
		$this->db->order_by('nome_produto', 'asc');
		$produtos = $this->db->get('cad_produtos', $where['limit'], $where['offset'])->result_array();

		$this->db->where(array('gratis' => 1));
		$pacotes = $this->db->get('pacote_ingresso', $where['limit'], $where['offset'])->result();

		return array(
			'pacotes' => $pacotes,
			'produtos' => $produtos
		);
		
	}

	public function listarMinhasRecompensas($id_usuario) {

		$this->db->select('id_produto, nome_produto, descricao_produto, nome_categoria, produto_usado, produto_recompensa_custo');
		$this->db->join('cad_categorias', 		 'cad_produtos.fk_categoria = cad_categorias.id_categoria', 'left');
		$this->db->join('cad_compra_recompensa as ccr', 'ccr.fk_produto = id_produto', 'inner');
		$this->db->where('produto_recompensa', 	 1);
		$this->db->where('ccr.fk_usuario', 			 $id_usuario);
		$this->db->order_by('nome_categoria', 	'asc');
		$this->db->order_by('nome_produto', 	'asc');
		return $this->db->get('cad_produtos')->result_array();

	}

	public function view_lista_leilao_pendente($where = null) {
		
		if ($where == null) {
			$where[0] = 10;
			$where[1] = 0;
		} else if (isset($where[0]) && !isset($where[1])) {
			$where[1] = 1;
			$where[1] = ($where[0] * $where[1]) - $where[0];
		} else {
			$where[1] = ($where[0] * $where[1]) - $where[0];
		}

		$this->db->where('status_leilao', 1);
		$leiloes = $this->db->get('view_leilao', $where[0], $where[1])->result();

		$status = $this->db->get('status_leilao')->result();

		return array('leiloes' => $leiloes, 'status' => $status);
	}

	public function view_lista_leilao_ativo($where = null) {
		
		if ($where == null) {
			$where[0] = 10;
			$where[1] = 0;
		} else if (isset($where[0]) && !isset($where[1])) {
			$where[1] = 1;
			$where[1] = ($where[0] * $where[1]) - $where[0];
		} else {
			$where[1] = ($where[0] * $where[1]) - $where[0];
		}

		$this->db->where('status_leilao', 2);
		$leiloes = $this->db->get('view_leilao', $where[0], $where[1])->result();

		$status = $this->db->get('status_leilao')->result();

		return array('leiloes' => $leiloes, 'status' => $status);
	}

	public function view_lista_leilao_finalizado($where = null) {
		
		if ($where == null) {
			$where[0] = 10;
			$where[1] = 0;
		} else if (isset($where[0]) && !isset($where[1])) {
			$where[1] = 1;
			$where[1] = ($where[0] * $where[1]) - $where[0];
		} else {
			$where[1] = ($where[0] * $where[1]) - $where[0];
		}

		$this->db->where('status_leilao', 4);
		$leiloes = $this->db->get('view_leilao', $where[0], $where[1])->result();

		$status = $this->db->get('status_leilao')->result();

		return array('leiloes' => $leiloes, 'status' => $status);
	}

	public function getLeiloes($valores) {

		// $this->db->select('id_leilao, id_produto, nome_produto, descricao_produto, valor_minimo, lance_minimo, ingressos, data_inicio , data_fim_previsto, ifnull(cad_favoritos.fk_usuario, 0) favorito, nome_usuario,avaliacao');
		
		// if(isset($valores['id_leilao'])){
		// 	$this->db->where('id_leilao', $valores['id_leilao']);	
		// }
		
		// $this->db->where('status_leilao', 2);
		// $this->db->where('view_leilao.fk_usuario <>', $valores['id_usuario']);
		// $this->db->join('cad_favoritos', 'view_leilao.id_leilao = cad_favoritos.fk_leilao', 'left');
		// //$this->db->order_by('data_inicio', 'desc');
		// $leiloes = $this->db->get('view_leilao', $valores['limit'], $valores['offset']);

		// // echo $this->db->last_query();
		// // die();
		
		// return $this->removeNullSub($leiloes->result_array());

		$this->db->select(" id_leilao,
						    id_produto,
						    nome_produto,
						    descricao_produto,
						    valor_inicial_lance as valor_minimo,
						    lance_minimo,
						    ingressos,
						    data_inicio,
						    produto_usado,
						    data_fim_previsto,
						    nome_usuario,
						    estado_usuario,
                            ifnull((SELECT count(*) from cad_favoritos where id_leilao = fk_leilao and id_usuario = cad_favoritos.fk_usuario),0) as favorito,
						    ifnull((SELECT avg(avaliacao) from cad_avaliacoes where fk_leiloeiro = fk_usuario),0) as avaliacao");

		$this->db->join('cad_produtos', 'cad_produtos.id_produto 	= cad_leilao.fk_produto', 	'inner');
		$this->db->join('seg_usuarios', 'seg_usuarios.id_usuario 	= cad_produtos.fk_usuario', 'inner');
		
		if(isset($valores['id_leilao'])){
			$this->db->where('id_leilao', $valores['id_leilao']);	
	   	} else {
			$this->db->where('status_leilao', 2);
			$this->db->where('data_fim_previsto >= ', 'current_timestamp', false); 
	   	}

		//$this->db->where('cad_produtos.fk_usuario <>', $valores['id_usuario']);
		$this->db->order_by('data_inicio', 'desc');
		$leiloes = $this->db->get('cad_leilao', $valores['limit'], $valores['offset']);

		// echo $this->db->last_query();
		// die();

		return $this->removeNullSub($leiloes->result_array());


	}

	public function meusLeiloes($valores) {

		$this->db->select('id_leilao, id_produto, nome_produto, descricao_produto, round(valor_minimo,2) as valor_minimo, round(lance_minimo,2) as lance_minimo, ingressos, data_inicio, data_fim_previsto, status_leilao.status status_leilao, produto_usado');
		$this->db->where('view_leilao.fk_usuario', $valores['id_usuario']);
		$this->db->join('status_leilao', 'view_leilao.status_leilao = status_leilao.id_status_leilao', 'left');
		$this->db->order_by('data_inicio', 'desc');
		$leiloes = $this->db->get('view_leilao', $valores['limit'], $valores['offset'])->result_array();

		return $this->removeNullSub($leiloes);
	}

	// public function ultimosLances($id_leilao) {
	// 	return $this->db->query("select valor, nome_usuario from cad_lances  limit 5")->result_array();
	// }

	public function consultaSala($valores) {
		$this->db->where($valores);
		return $this->db->get('sala_leilao');
	}

	public function favorito($id_usuario, $id_leilao) {
		
		$this->db->where(array('fk_usuario' => $id_usuario, 'fk_leilao' => $id_leilao));
		
		if ($this->db->get('cad_favoritos')->num_rows() > 0) { // verifica se o usuário já tem o leilão como favorito
			
			$this->db->where(array('fk_usuario' => $id_usuario, 'fk_leilao' => $id_leilao));
			$this->db->delete('cad_favoritos'); // se tiver deleta
			return 'Leilão removido dos favoritos';

		} else {

			$this->db->insert('cad_favoritos', array('fk_usuario' => $id_usuario, 'fk_leilao' => $id_leilao)); // se não tiver insere
			return 'Leilão adicionado aos favoritos';
		}
	}

	public function listarFavoritos($valores) {
		
		$this->db->select('id_leilao, id_produto, nome_usuario, nome_produto, descricao_produto, valor_inicial_lance as valor_minimo, lance_minimo, ingressos, data_inicio, data_fim_previsto, ifnull(cad_favoritos.fk_usuario, 0) favorito, ifnull((SELECT avg(avaliacao) from cad_avaliacoes where fk_leiloeiro = view_leilao.fk_usuario),0) as avaliacao, produto_usado');
		$this->db->where('status_leilao', 2);
		$this->db->where('cad_favoritos.fk_usuario', $valores['id_usuario']);
		$this->db->join('view_leilao', 'cad_favoritos.fk_leilao = view_leilao.id_leilao', 'left');
		$this->db->order_by('data_inicio', 'desc');
		$favoritos = $this->db->get('cad_favoritos', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($favoritos);
	}

	public function listarArrematados($valores) {
		
		$this->db->select('id_leilao, id_produto, nome_produto, descricao_produto, valor_minimo, lance_minimo, ingressos, data_inicio, data_fim_previsto, ifnull((select avaliacao from cad_avaliacoes ca where ca.fk_leilao = id_leilao), 0) avaliado,produto_usado, lance_formatado as ultimo_lance');
		$this->db->where('status_leilao', 4);
		$this->db->where('fk_usuario_arrematou', $valores['id_usuario']);
		$this->db->order_by('data_inicio', 'desc');
		$arrematados = $this->db->get('view_leilao', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($arrematados);
	}

	public function listarMeusArrematados($valores){
		$this->db->select('id_leilao, id_produto, nome_produto, descricao_produto, valor_minimo, lance_minimo, ingressos, data_inicio, data_fim_previsto, ifnull((select avaliacao from cad_avaliacoes ca where ca.fk_leilao = id_leilao), 0) avaliado,produto_usado, lance_formatado as ultimo_lance, nome_usuario_arrematou,
			(select email_usuario from seg_usuarios where id_usuario = fk_usuario_arrematou) as email_usuario_arrematou,
			(select telefone_usuario from seg_usuarios where id_usuario = fk_usuario_arrematou) as telefone_usuario_arrematou');
		$this->db->where('status_leilao', 4);
		$this->db->where('fk_usuario', $valores['id_usuario']);
		$this->db->order_by('data_inicio', 'desc');
		$arrematados = $this->db->get('view_leilao', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($arrematados);
	}

	public function listarAfiliados($valores) {
		
		$this->db->select("nome_usuario, date_format(criacao_usuario,'%d/%m/%Y às %H:%i:%s') as criado_em");
		$this->db->where('usuario_criou_usuario', $valores['id_usuario']);
		$this->db->order_by('criacao_usuario', 'desc');
		$arrematados = $this->db->get('seg_usuarios', $valores['limit'], $valores['offset'])->result_array();
		return $this->removeNullSub($arrematados);
	}

	//Não são os ingressos,
	public function getCreditos($id){
		$creditos = $this->db->query('select ifnull(creditos,0) as creditos from cad_creditos where fk_usuario = '.$id);
		if (isset($creditos) && !is_null($creditos) && $creditos->num_rows() == 1) {
			return $creditos->row()->creditos;
		} else {
			return 0;
		}
	}

	public function getLeiloesCategorias($valores) {

		$arvore = $this->arvore_categorias($valores['id_categoria']);

		$this->recursiva($arvore);


		$this->db->select(" id_leilao,
						    id_produto,
						    nome_produto,
						    descricao_produto,
						    valor_inicial_lance as valor_minimo,
						    lance_minimo,
						    ingressos,
						    data_inicio,
						    produto_usado,
						    data_fim_previsto,
						    nome_usuario,
						    estado_usuario,
                            ifnull((SELECT count(*) from cad_favoritos where id_leilao = fk_leilao and id_usuario = cad_favoritos.fk_usuario),0) as favorito,
						    ifnull((SELECT avg(avaliacao) from cad_avaliacoes where fk_leiloeiro = fk_usuario),0) as avaliacao");

		if(isset($valores['id_leilao'])){
		 	$this->db->where('id_leilao', $valores['id_leilao']);	
		}

		$this->db->join('cad_produtos', 'cad_produtos.id_produto 	= cad_leilao.fk_produto', 	'inner');
		$this->db->join('seg_usuarios', 'seg_usuarios.id_usuario 	= cad_produtos.fk_usuario', 'inner');
		//$this->db->where('cad_produtos.fk_usuario <>', $valores['id_usuario']);
		$this->db->where('status_leilao', 2);
		$this->categorias[] = $valores['id_categoria']; // garante que a categoria recebida está na lista
		if($this->categorias != "" && $this->categorias != null && $this->categorias){
			$this->db->where_in('fk_categoria', $this->categorias);
		}
		
		$this->db->where('data_fim_previsto >= ', 'current_timestamp', false); 

		$this->db->order_by('data_inicio', 'desc');
		$leiloes = $this->db->get('cad_leilao', $valores['limit'], $valores['offset'])->result_array();

		//echo $this->db->last_query();
		//die();

		return $this->removeNullSub($leiloes);
	}

	public function recursiva($array) {
		foreach ($array as $key => $value) {
			$this->categorias[] = $value['id_categoria'];
			$this->recursiva($value['subcategorias']);
		}
	}

	public function buscarLeiloes($valores) {

		$this->db->select(" id_leilao,
						    id_produto,
						    nome_produto,
						    descricao_produto,
						    valor_inicial_lance as valor_minimo,
						    lance_minimo,
						    ingressos,
						    data_inicio,
						    produto_usado,
						    data_fim_previsto,
						    nome_usuario,
						    estado_usuario,
                            ifnull((SELECT count(*) from cad_favoritos where id_leilao = fk_leilao and id_usuario = cad_favoritos.fk_usuario),0) as favorito,
						    ifnull((SELECT avg(avaliacao) from cad_avaliacoes where fk_leiloeiro = fk_usuario),0) as avaliacao");

		if(isset($valores['id_leilao'])){
		 	$this->db->where('id_leilao', $valores['id_leilao']);	
		}

		$this->db->join('cad_produtos', 'cad_produtos.id_produto 	= cad_leilao.fk_produto', 	'inner');
		$this->db->join('seg_usuarios', 'seg_usuarios.id_usuario 	= cad_produtos.fk_usuario', 'inner');
		$this->db->where('status_leilao', 2);
		$this->db->where('data_fim_previsto >=', 'current_timestamp', false); //unquoted
		//$this->db->where('cad_produtos.fk_usuario <>', $valores['id_usuario']);
		$this->db->where("(lower(nome_produto) LIKE lower('%{$valores['busca']}%') ESCAPE '!'
						OR  lower(descricao_produto) LIKE lower('%{$valores['busca']}%') ESCAPE '!')");
		$this->db->order_by('data_inicio', 'desc');
		$leiloes = $this->db->get('cad_leilao', $valores['limit'], $valores['offset'])->result_array();

		return $this->removeNullSub($leiloes);
	}

	public function historicoLances($valores){

		$this->db->select("	id_leilao,
						    nome_produto,
						    format(valor,2,'de_DE') as valor,
						    date_format(data_lance,'%d/%m/%Y às %H:%i:%s') as data");

		$this->db->where('id_usuario', $valores['id_usuario']);
		$this->db->like('nome_produto', $valores['nome_produto'], 'BOTH');

		
		$this->db->join('sala_leilao sl', 	'id_sala_leilao = fk_sala_leilao', 	'inner');
		$this->db->join('cad_leilao', 		'id_leilao = fk_leilao', 			'inner');
		$this->db->join('cad_produtos', 	'id_produto = fk_produto', 			'inner');
		$this->db->join('seg_usuarios', 	'id_usuario = sl.fk_usuario', 		'inner');

		$this->db->order_by('data_lance', 'desc');
		$this->db->order_by('valor', 'desc');
		$lances = $this->db->get('cad_lances', $valores['limit'], $valores['offset'])->result_array();

		return $this->removeNullSub($lances);

	}

	public function getArrematados() {
		
		/*No HAVING a sub-query traz o valor sem formatação*/
		$pendentes = $this->db->query("SELECT 
			id_leilao, nome_produto, descricao_produto, id_produto,valor_minimo,
			format((select max(valor) 
							from cad_lances 
								inner join sala_leilao on fk_sala_leilao = id_sala_leilao
								where fk_leilao = id_leilao),2,'de_DE') as valor
				FROM cad_leilao cl 
					inner join cad_produtos on id_produto = cl.fk_produto
					WHERE status_leilao > 1 
						and status_leilao <> 4 
						and data_fim_previsto < CURRENT_TIMESTAMP 
						and fk_usuario_arrematou is null
						HAVING valor is not null
							and (select max(valor) 
							from cad_lances 
								inner join sala_leilao on fk_sala_leilao = id_sala_leilao
								where fk_leilao = id_leilao) >= cl.valor_minimo")->result();

		$arrematados = array();						

		foreach($pendentes as $pendente){

			$usuario = $this->db->query("select id_usuario, email_usuario, nome_usuario 
											from cad_lances 
												inner join sala_leilao on fk_sala_leilao = id_sala_leilao 
												inner join seg_usuarios on id_usuario = fk_usuario 
													where fk_leilao = {$pendente->id_leilao} 
														order by valor desc 
														limit 1 ");
			
			log_message('debug',$this->db->last_query());
				
			if($usuario->num_rows() == 1){

				array_push($arrematados,array(
					'id_leilao' => $pendente->id_leilao,
					'nome_produto' => $pendente->nome_produto,
					'descricao_produto' => $pendente->descricao_produto,
					'id_produto' => $pendente->id_produto,
					'valor' => $pendente->valor,
					'id_usuario' => $usuario->row()->id_usuario,
					'email_usuario' => $usuario->row()->email_usuario,
					'nome_usuario' => $usuario->row()->nome_usuario
				));

				$this->db->query("update cad_leilao 
									set status_leilao = 4, 
									fk_usuario_arrematou = {$usuario->row()->id_usuario} 
									where 
										id_leilao = {$pendente->id_leilao}");

				log_message('debug',$this->db->last_query());

			}
		}

		return $arrematados;

	}

	public function cron_Push(){

		return $this->db->get("view_notificar_leiloes");

	}

	public function acetarRejeitar($aceitou,$leilao){
		return $this->db->query("update cad_leilao set status_leilao = {$aceitou} where id_leilao = {$leilao}");
	}

	public function view_novo_leiloes(){
		return $this->db->get_where('cad_produtos',array('fk_usuario' => $this->session->userdata('usuario')))->result();
	}

	public function view_editar_leilao($where = null){

		$leilao = $this->db->select("*,round(lance_minimo,2) as lance_minimo,round(valor_minimo,2) as valor_minimo,round(valor_inicial_lance,2) as valor_inicial_lance , concat(date_format(data_inicio,'%d/%m/%Y %H:%i'),' até ',date_format(data_fim_previsto,'%d/%m/%Y %H:%i')) as periodo, altura,largura,comprimento,peso ")->get_where('cad_leilao', array('id_leilao' => $where[0]))->row();

		if (isset($leilao)) {
			foreach ($leilao as $key => $value) {
				$this->session->set_flashdata("{$key}_edicao",$value);
			}
		}

		return $this->db->get('cad_produtos')->result();
	}

	public function validandoPagamento($id_leilao){
		$this->db->query("update cad_leilao set finalizado = true where id_leilao = {$id_leilao}");
	}

	######################################################	
	//Avaliar
	######################################################	
	public function avaliar($dados){

		return $this->db->query("insert into cad_avaliacoes (fk_cliente,fk_leiloeiro,avaliacao,fk_leilao) values 
									({$dados['id_usuario']},(select fk_usuario from cad_leilao inner join cad_produtos on id_produto = fk_produto where id_leilao = {$dados['id_leilao']}),{$dados['avaliacao']},{$dados['id_leilao']})");


	}

	public function notificado($id_leilao) {

		return $this->db->query("update cad_leilao set notificado = (ifnull(notificado,0)+1) where id_leilao = {$id_leilao}");

	}

}

/* End of file Model_leiloes.php */
/* Location: ./application/models/Model_leiloes.php */