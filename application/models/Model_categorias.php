<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_categorias extends MY_Model {

	public function view_categorias($where) {

		$categorias = $this->arvore_categorias();

		return $this->removeNullSub($categorias);
	}

	public function create($categoria) {
		return $this->db->insert('cad_categorias',$categoria);
	}

	public function listaCategorias($id_categoria = null) {

		$this->db->select('id_categoria, nome_categoria, ativa, fk_categoria_pai');
		$this->db->where('fk_categoria_pai', $id_categoria);
		$this->db->where('ativa', true);
		$this->db->order_by('nome_categoria', 'asc');
		$categorias = $this->db->get('cad_categorias')->result_array();

		$categorias = $this->removeNullSub($categorias);

		return $categorias;
	}
}

/* End of file Model_categorias.php */
/* Location: ./application/models/Model_categorias.php */
