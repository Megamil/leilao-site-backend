<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_pacotes extends MY_Model {

		public function validaPagamento($id_pacote_ingresso){

			$pagamento =  $this->db->query("SELECT ifnull(finalizado,0) as finalizado 
			from cad_compra_pacotes 
				where id = {$id_pacote_ingresso}");

			if (isset($pagamento) && !is_null($pagamento) && $pagamento->num_rows() == 1) {
				$pagamento->row()->finalizado;
			} else {
				return 0;
			}

		}

		public function validaPagamentoProduto($id_leilao){

			$pagamento =  $this->db->query("SELECT ifnull(finalizado,0) as finalizado 
			from cad_leilao 
				where id_leilao = {$id_leilao}");

			if (isset($pagamento) && !is_null($pagamento) && $pagamento->num_rows() == 1) {
				$pagamento->row()->finalizado;
			} else {
				return 0;
			}

		}

		public function finalizarPagamento($id_pacote_ingresso){

			return $this->db->query("UPDATE cad_compra_pacotes 
										SET finalizado = true
											where id = {$id_pacote_ingresso}");

		}

		public function finalizarLeilao($id_leilao){

			return $this->db->query("UPDATE cad_leilao 
										SET finalizado = true
											where id_leilao = {$id_leilao}");

		}

		public function pacote($id_pacote_ingresso) {
			return $this->db->query("select descricao_pacote, quantidade_ingresso, round(valor_pacote,2) as valor_pacote from pacote_ingresso where id_pacote_ingresso = {$id_pacote_ingresso}")->row();
		}

		public function dadosCliente($id_usuario){
			return $this->db->query("
				SELECT nome_usuario, criacao_usuario, celular_usuario, email_usuario
				from seg_usuarios
						where id_usuario = {$id_usuario}")->row();
		}

		public function insertCompra($dados){
			$this->db->insert('cad_compra_pacotes',$dados);
			return $this->db->insert_id();
		}

		public function insertHistCompra($dados){

			$dados['date_approved'] = $this->fixHour($dados['date_approved']);
			$dados['date_created'] 	= $this->fixHour($dados['date_created']);
			$dados['last_modified'] = $this->fixHour($dados['last_modified']);

			$this->db->insert('hist_compra_pacotes',$dados);
			return $this->db->insert_id();
			
		}

		public function fixHour($hour) {
			
			if(!isset($hour) || $hour == '') {return null;}

			$hour_ = explode(".",$hour);
			return str_replace("T"," ",$hour_[0]);
			
		}

		public function update_Consulta_Preference($id_consulta_preference,$topic,$id){

			$this->db->where(array('id' => $id));
			return $this->db->update('cad_compra_pacotes',array('id_consulta_preference' => $id_consulta_preference, 'topic' => $topic));

		}

		public function update_Consulta_Preference_leilao($id_consulta_preference,$topic,$id){

			$this->db->where(array('id_leilao' => $id));
			return $this->db->update('cad_leilao',array('id_consulta_preference' => $id_consulta_preference, 'topic' => $topic));

		}

		public function link_compra($url, $id){
			$this->db->where(array('id' => $id));
			return $this->db->update('cad_compra_pacotes',array('link_compra' => $url));
		}

		public function consultarPacote($id_pacote_ingresso){
			return $this->db->get_where('cad_compra_pacotes', array('id' => $id_pacote_ingresso))->row();
		}

		public function view_lista_pacote(){
			return array('pacotes' => $this->db->get('pacote_ingresso')->result());
		}

		public function view_editar_pacote($where = null){
			
			$pacote = $this->db->select('*, round(valor_pacote,2) as valor_pacote')->get_where('pacote_ingresso', array('id_pacote_ingresso' => $where[0]))->row();

			if (isset($pacote)) {
				foreach ($pacote as $key => $value) {
					$this->session->set_flashdata("{$key}_edicao",$value);
				}
			}

			return array();

		}

		public function view_lista_pacotes(){
			return array('pacotes' => $this->db->select('*, round(valor_pacote,2) as valor_pacote')->get('pacote_ingresso')->result());
		}

		public function create($valores = null){

			$this->db->insert('pacote_ingresso',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_pacotes / create';
				return 100;		
			} else {
				return $this->db->insert_id();
			}

		}

		public function update($valores = null){

			//Alterar
			$tabela = "pacote_ingresso";
			$id = 'id_pacote_ingresso';
			
			$this->gerarHistorico($id,$tabela,$valores);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);	

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_pacotes / update';
				return false;		
			} else {
				return true;
			}

		}

		public function view_vendas_pacote(){

			return array('vendas' => $this->db->query("SELECT
														id,
													    descricao_pacote,
													    round(unit_price,2) as unit_price,
													    cad_compra_pacotes.quantidade_ingresso,
													    nome_usuario,
													    date_format(date_buy,'%d/%m/%Y às %H:%i:%s') as date_buy,
													    finalizado, 
													    id_consulta_preference as preference
													    	from cad_compra_pacotes
													        	inner join pacote_ingresso on id_pacote_ingresso = fk_pacote
													            inner join seg_usuarios on id_usuario = fk_usuario")->result());

		}

		public function vendas_pacote($fk_usuario){

			return array('vendas' => $this->db->query("SELECT
														id,
													    descricao_pacote,
													    round(unit_price,2) as unit_price,
													    ccp1.quantidade_ingresso,
													    nome_usuario,
													    date_format(date_buy,'%d/%m/%Y às %H:%i:%s') as date_buy,
													    finalizado, 
													    id_consulta_preference as preference,
													    (SELECT 
															status
														    	from hist_compra_pacotes 
														        inner join cad_compra_pacotes ccp2 on ccp2.id = fk_compra_pacotes
														        	where id_consulta_preference = ccp1.id_consulta_preference
														            order by last_modified desc
														            limit 1) as status
													    	from cad_compra_pacotes ccp1
													        	inner join pacote_ingresso on id_pacote_ingresso = fk_pacote
													            inner join seg_usuarios on id_usuario = fk_usuario
													            	where id_usuario = {$fk_usuario}")->result());

		}

		public function detalhes_vendas_pacote($fk_compra_pacotes){

			return $this->db->query("SELECT 
															
										status,
										status_detail,
										date_format(date_approved,'%d/%m/%Y às %H:%i:%s') as date_approved,
										date_format(date_created,'%d/%m/%Y às %H:%i:%s') as date_created,
										date_format(last_modified,'%d/%m/%Y às %H:%i:%s') as last_modified

										FROM hist_compra_pacotes
											where fk_compra_pacotes = {$fk_compra_pacotes}")->result();

		}

		public function validarPendentes($id_usuario = null){

			$where = !is_null($id_usuario) ? " and fk_usuario = {$id_usuario}" : "";

			return $this->db->query("SELECT ccp.id_consulta_preference as id, ccp.topic 
										from cad_compra_pacotes ccp
											where ccp.id_consulta_preference is not null 
												and ccp.id_consulta_preference <> ''
													and ccp.finalizado = false
													{$where}
													and (select count(*) 
															from hist_compra_pacotes 
																where fk_compra_pacotes = ccp.id 
																and (status = 'pending' or status = 'in_process')) > 0")->result();
			/*Todo Pensar numa forma de limitar essas tentativas*/
		}

		public function validarPendentesProdutos($id_usuario = null){

			$where = !is_null($id_usuario) ? " and fk_usuario_arrematou = {$id_usuario}" : "";

			return $this->db->query("SELECT cl.id_consulta_preference as id, cl.topic 
										from cad_leilao cl
											where cl.id_consulta_preference is not null 
												and cl.id_consulta_preference <> ''
													and (cl.finalizado = false or cl.finalizado is null)
													{$where}")->result();
			/*Todo Pensar numa forma de limitar essas tentativas*/
		}


	}