<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_webservice extends MY_Model {

		public function validar_login($valores){

			$token_acesso = $this->gerarToken();
			$this->db->query("UPDATE seg_usuarios set token_acesso = CONCAT('{$token_acesso}', id_usuario) WHERE email_usuario = '{$valores['email_usuario']}'");

			$this->db->select('id_usuario,TO_BASE64(id_usuario) as code, nome_usuario, email_usuario, celular_usuario, telefone_usuario, fk_grupo_usuario, ativo_usuario, token_acesso, ativado_sms, (select bloquear from view_completar_perfil vcp where vcp.id_usuario = ss.id_usuario) as bloquear');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('email_usuario',$valores['email_usuario']);
			$this->db->where('senha_usuario',$valores['senha_usuario']);
			$login = $this->db->get('seg_usuarios ss');

			if (isset($login) && !is_null($login) && $login->num_rows() == 1) {
				return $this->removeNull($login->row());
			} else {
				return false;
			}

		}

		public function validar_login_facebook($fbid){

			$token_acesso = $this->gerarToken();
			$this->db->query("UPDATE seg_usuarios set token_acesso = CONCAT('{$token_acesso}', id_usuario) WHERE fbid = '{$fbid}'");

			$this->db->select('id_usuario,TO_BASE64(id_usuario) as code, nome_usuario, email_usuario, celular_usuario, telefone_usuario, fk_grupo_usuario, ativo_usuario, token_acesso, ativado_sms, (select bloquear from view_completar_perfil vcp where vcp.id_usuario = ss.id_usuario) as bloquear');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('fbid', $fbid);
			$login = $this->db->get('seg_usuarios ss');

			if (isset($login) && !is_null($login) && $login->num_rows() == 1) {
				return $this->removeNull($login->row());
			} else {
				return false;
			}

		}

		public function validar_token($token){

			$this->db->select('id_usuario, nome_usuario, email_usuario, celular_usuario, telefone_usuario, fk_grupo_usuario, ativo_usuario, ativado_sms, (select bloquear from view_completar_perfil vcp where vcp.id_usuario = ss.id_usuario) as bloquear');
			$this->db->join('seg_grupos','fk_grupo_usuario = id_grupo');
			$this->db->where('token_acesso', $token);
			$login = $this->db->get('seg_usuarios ss');

			if (isset($login) && !is_null($login) && $login->num_rows() == 1) {
				return $this->removeNull($login->row());
			} else {
				return false;
			}

		}

		public function ativarSms($id_usuario){
			$this->db->where('id_usuario', $id_usuario);
			return $this->db->update('seg_usuarios', array('ativado_sms' => 1));
		}

		######################################################	
		//Pré Editar Usuário
		######################################################
		public function preEditarUsuario($id_usuario) {
			$this->db->select('id_usuario, nome_usuario, sexo_usuario, rg_usuario, cpf_usuario, celular_usuario, telefone_usuario, cep_usuario, logradouro_usuario, numero_usuario, bairro_usuario, cidade_usuario, estado_usuario, complemento_usuario, email_usuario');
			$this->db->where('id_usuario', $id_usuario);
			$usuario = $this->db->get('seg_usuarios')->row_array();

			return $this->removeNull($usuario);
		}

		######################################################	
		//Editar Usuário
		######################################################	
		public function editarUsuario($valores){

			$valores = $this->limpa_array($valores);

			$tabela = "seg_usuarios";
			$id = 'id_usuario';
			
			$this->gerarHistorico($id,$tabela,$valores,$valores[$id]);
			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);

			return $this->verificarErros($this->db->error(),'Model_webservice / editarUsuario');	
		}
		######################################################	
		//Criar Usuário
		######################################################	
		public function criarUsuario($valores){

			$this->db->insert('seg_usuarios',$valores);

			//Não são os ingressos,
			$this->db->query("insert into cad_creditos (fk_usuario,creditos) values ({$this->db->insert_id()},0)");
			return $this->verificarErros($this->db->error(),'Model_webservice / criarUsuario');	


		}

		//Não são os ingressos,
		public function inserirCredito($id){
			return $this->db->query("update cad_creditos set creditos = (creditos+1) where fk_usuario = {$id}");
		}
		######################################################	
		//Listar Usuários
		######################################################	
		public function listarUsuarios(){

			return $this->db->select('nome_usuario,email_usuario')
					 		->get('seg_usuarios')
					 		->result();


		}

		public function validarNumCode($id_usuario){
			return $this->db->select('*')
							->where('id_usuario',$id_usuario)
							->get('seg_usuarios');
		}

		######################################################	
		//Gerar token
		######################################################
		function gerarToken($token_acesso = null) {

			if (!is_null($token_acesso)) {
				return $token_acesso;
			}

			$token_acesso = sha1(uniqid(rand(), true));
			
			if ($this->db->limit(1)->get_where('seg_usuarios', array('token_acesso' => $token_acesso))->num_rows() === 0) {
				return $token_acesso;
			} else {
				$this->gerarToken();
			}


		}

		######################################
		//Notificações PUSH
		######################################
		public function carregarFirebaseTokens($id_usuario = null) {

			$dados = $this->db->query("select distinct(token) as token,fk_usuario
												from cad_tokens
													where fk_usuario = {$id_usuario}");

			return $dados;

		}

		public function atualizarInserirToken($id_usuario,$aparelho,$token){

			$id_token = $this->db->query("select id_token
										from cad_tokens
											where fk_usuario = {$id_usuario}
												and aparelho = {$aparelho}")->row();

			if(isset($id_token)){

				return $this->db->query("update cad_tokens set token = '{$token}' where id_token = {$id_token->id_token};");

			} else {
				return $this->db->query("insert into cad_tokens (fk_usuario,aparelho,token)
											values ({$id_usuario},{$aparelho},'{$token}');");
			}


		}

		public function deletarToken($token = null) {

			$this->db->where('token',$token);
			return $this->db->delete("cad_tokens");

		}

		public function atualizarToken($novo = null, $antigo = null) {

			return $this->db->query("update cad_tokens set token = '".$novo."' where token = '".$antigo."';");

		}

		public function getPendencias($id_usuario){

			return $this->db->query("SELECT 
										id_leilao, nome_produto 
											FROM cad_leilao 
												inner join cad_produtos on id_produto = fk_produto
													WHERE fk_usuario_arrematou = {$id_usuario} and (finalizado = 0 or finalizado is null)")->result_array();

		}

		public function filtroUf($uf) {
			return $this->db->query("SELECT id_item_grupo as id FROM db_leilao.cad_item_grupo where nome_item_grupo = '{$uf}'")->row()->id;
		}

	}


?>