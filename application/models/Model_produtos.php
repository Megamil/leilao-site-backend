<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_produtos extends MY_Model {

	######################################################	
	//Criar Produto
	######################################################	
	public function criarProduto($valores){

		$this->db->insert('cad_produtos',$valores);
		$id = $this->db->insert_id();
		
		if ($this->verificarErros($this->db->error(),'Model_produtos / criarProduto')) {
			return $id;
		}

		return false;
	}

	######################################################	
	//Listar Produto
	######################################################
	public function listarProduto($id_usuario) {
		
		$this->db->select('id_produto, nome_produto, descricao_produto, nome_categoria, produto_usado, fk_categoria');
		$this->db->join('cad_categorias', 'cad_produtos.fk_categoria = cad_categorias.id_categoria', 'left');
		$this->db->where('fk_usuario', $id_usuario);
		$this->db->order_by('nome_categoria', 'asc');
		$this->db->order_by('nome_produto', 'asc');
		return $this->removeNullSub($this->db->get('cad_produtos')->result_array());
	}

	######################################################	
	//Buscar Produto
	######################################################
	public function buscarProduto($id_produto) {
		
		$this->db->where('id_produto', $id_produto);
		return $this->removeNull($this->db->get('cad_produtos')->row());
	}

	//Dashboard
	public function view_lista_produtos(){
		return array('produtos' => $this->db->query("select nome_categoria, id_produto, nome_produto, descricao_produto, produto_usado, produto_recompensa, produto_recompensa_custo 
														from cad_produtos 
													    inner join 
													    	cad_categorias on id_categoria = fk_categoria
													    		where fk_usuario = {$this->session->userdata('usuario')}")->result());
	}

	//Dashboard
	public function view_vendas_produtos(){
		return 	array('leiloes' => 
						$this->db->select("date_format(data_fim_previsto,'%d/%m/%Y às  %H:%i:%s') as data_fim_previsto, id_leilao,nome_produto,fk_usuario_arrematou,nome_usuario_arrematou,lance_formatado,nome_usuario_leiloando,finalizado,link_compra,id_consulta_preference,topic")
								 ->where('status_leilao',4)
								 ->where('finalizado <>',1)
								 ->where('fk_usuario_arrematou is not null',null)
								 ->get('view_leilao')
								 ->result()
				);
	}

	public function view_listar_recompensas(){

		return $this->db->query("SELECT	
									id_compra_recompensa,
									id_usuario,
								    id_produto,
									date_format(data_compra,'%d/%m/%Y às  %H:%i:%s') data_compra,
								    nome_usuario,
								    email_usuario,
								    celular_usuario,
								    nome_produto
								    
								    from cad_compra_recompensa
								    	inner join seg_usuarios on id_usuario = fk_usuario
								    	inner join cad_produtos on id_produto = fk_produto
								    	where entregue = false")->result();
		
	}

	public function marcarEntrega($cod){
		$this->db->query("update cad_compra_recompensa set data_entrega = current_timestamp, entregue = true where id_compra_recompensa = {$cod}");

		return $this->db->query("SELECT id_usuario, nome_produto 
									from cad_compra_recompensa
								    	inner join seg_usuarios on id_usuario = fk_usuario
								    	inner join cad_produtos on id_produto = fk_produto
								    		where id_compra_recompensa = {$cod}")->row();

	}

	public function view_novo_produto(){
		return array('categorias' => $this->db->get('cad_categorias')->result());
	}

	public function view_editar_produto($where){
		
		$produto = $this->db->get_where('cad_produtos', array('id_produto' => $where[0]))->row();

		if (isset($produto)) {
			foreach ($produto as $key => $value) {
				$this->session->set_flashdata("{$key}_edicao",$value);
			}
		}

		return array('categorias' => $this->db->get('cad_categorias')->result());
	}

	public function create($valores = null){

		$this->db->insert('cad_produtos',$valores);

		$e = $this->db->error();
		if ($e['code'] != 0) {
			$this->code = $e['code'];
			$this->message = $e['message'];	
			$this->query = $this->db->last_query();
			$this->funcao = 'Model_produtos / create';
			return 100;		
		} else {
			return $this->db->insert_id();
		}

	}

	public function update($valores = null){

		//Alterar
		$tabela = "cad_produtos";
		$id = 'id_produto';
		
		$this->gerarHistorico($id,$tabela,$valores);
		$this->db->where(array($id => $valores[$id]));
		$this->db->update($tabela,$valores);	

		$e = $this->db->error();
		if ($e['code'] != 0) {
			$this->code = $e['code'];
			$this->message = $e['message'];	
			$this->query = $this->db->last_query();
			$this->funcao = 'Model_produtos / update';
			return false;		
		} else {
			return true;
		}

	}

	public function dadosCliente($id_usuario){
		return $this->db->query("
			SELECT nome_usuario, criacao_usuario, celular_usuario, email_usuario, cep_usuario
			from seg_usuarios
					where id_usuario = {$id_usuario}")->row();
	}

	public function getProduto($id_leilao){
		return $this->db->query("
							SELECT 
								id_produto, 
								nome_produto, 
								descricao_produto, 
								cep_usuario, 
								peso, 
								altura, 
								largura, 
								comprimento, 
								link_compra,
								(select max(valor) 
										from cad_lances 
											inner join sala_leilao on fk_sala_leilao = id_sala_leilao
											where fk_leilao = id_leilao) as valor
							from cad_leilao
								inner join cad_produtos on id_produto = fk_produto
								inner join seg_usuarios on id_usuario = fk_usuario
									where id_leilao = {$id_leilao}")->row();
	}

	public function link_compra($link_compra,$id_leilao){
		return $this->db->query("UPDATE cad_leilao 
										SET link_compra = '{$link_compra}'
											where id_leilao = {$id_leilao}");
	}

}

/* End of file Model_produtos.php */
/* Location: ./application/models/Model_produtos.php */