<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class MY_Model extends CI_Model {

		private $code = null;
		private $message = null;
		private $query = null;
		private $funcao = null;

		protected $categorias = array();

		function __construct() {
		    parent::__construct();
		}

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

				$maquina_usuario_erro = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;

			    $erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $maquina_usuario_erro
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}
		

		public function gerarHistorico($id,$tabela,$valores,$id_usuario = null){

			$comparar = $this->db->get_where($tabela,array($id => $valores[$id]))->row_array();

			foreach ($valores as $key => $valor) {
				if ($valor != $comparar[$key]) {
					$log = array (
								'fk_usuario'=> isset($id_usuario) ? $id_usuario : $this->session->userdata('usuario'),
								'original_edicao'=> $comparar[$key],
								'fk_aplicacao'=> $this->session->userdata('id_aplicacao_atual'),
								'novo_edicao'=> "{$valor}",
								'campo_edicao'=> "{$key}",
								'tabela_edicao'=> $tabela,
								'id_edicao'=> $valores[$id],
							);

					$this->db->insert('seg_log_edicao',$log);
				} 
			}

		}

		public function verificarErros($erro,$funcao){

			if ($erro['code'] != 0) {
				$this->code = $erro['code'];
				$this->message = $erro['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = $funcao;
				return false;		
			} else {
				return true;
			}

		}

		######################################################	
		// Remove NULL
		######################################################
		protected function removeNull($values) {

			if (is_array($values)) {
				foreach ($values as $key => $value) {
					if (is_null($value)) {
						$values[$key] = '';
					}
				}
			} else if (is_object($values)) {
				foreach ($values as $key => $value) {
					if (is_null($value)) {
						$values->$key = '';
					}
				}
			}

			return $values;
		}

		######################################################	
		// Remove NULL sub 
		######################################################
		protected function removeNullSub($values) {

			if (is_array($values)) {
				foreach ($values as $key => $values2) {
					foreach ($values2 as $key2 => $value2) {
						if (is_null($value2)) {
							$values[$key][$key2] = '';
						}
					}
				}
			} else if (is_object($values)) {
				foreach ($values as $key => $values2) {
					foreach ($values2 as $key2 => $value2) {
						if (is_null($value2)) {
							$values->$key->$key2 = '';
						}
					}
				}
			}

			return $values;
		}

	protected function limpa_array($valores) {
		foreach ($valores as $key => $value) {
			if (is_null($value) || $value == '') {
				unset($valores[$key]);
			}
		}
		return $valores;
	}

	protected function arvore_categorias($categoria_pai = null) {

		$this->db->select('id_categoria, nome_categoria, ativa, fk_categoria_pai, ifnull((select nome_categoria from cad_categorias cc2 where cc2.id_categoria = cc.fk_categoria_pai),\'Sem\') as categoria_pai');
		if(!is_null($categoria_pai)){
			$this->db->where('fk_categoria_pai', $categoria_pai);
		}
		$this->db->where('ativa', true);
		$categorias = $this->db->get('cad_categorias cc')->result_array();

		foreach ($categorias as $key => $categoria) {
			$categorias[$key]['subcategorias'] = $this->arvore_categorias($categoria['id_categoria']);
		}

		return $categorias;
	}

}