<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_pacotes extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_pacotes');
		    
	}



	public function novo_pacote(){

		$this->form_validation->set_rules('descricao_pacote','Grupo do Usuário','required');
		$this->form_validation->set_rules('quantidade_ingresso','Grupo do Usuário','required');
		$this->form_validation->set_rules('valor_pacote','Grupo do Usuário','required');
		$this->form_validation->set_rules('gratis','Grátis','required');

		$dados = array (
			'descricao_pacote'	 	=> $this->input->post('descricao_pacote'),
			'quantidade_ingresso' 	=> $this->input->post('quantidade_ingresso'),
			'valor_pacote' 			=> $this->input->post('valor_pacote'),
			'gratis' 				=> $this->input->post('gratis')
		);

		if ($this->form_validation->run()) {

			$this->model_pacotes->start();
			
			$id = $this->model_pacotes->create($dados);

			$commit = $this->model_pacotes->commit();

			if ($commit['status']) {
				$this->aviso('Pacote Criado','Pacote "'.$this->input->post('descricao_pacote').'" criado com sucesso!.','success',false);

				redirect('main/redirecionar/26/'.$id);
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/25');
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/25');

		}

	}

	public function editar_pacote(){

		$this->form_validation->set_rules('id_pacote_ingresso','ID','required');
		$this->form_validation->set_rules('descricao_pacote','Grupo do Usuário','required');
		$this->form_validation->set_rules('quantidade_ingresso','Grupo do Usuário','required');
		$this->form_validation->set_rules('valor_pacote','Grupo do Usuário','required');
		$this->form_validation->set_rules('gratis','Grátis','required');

		$dados = array (
			'id_pacote_ingresso'	=> $this->input->post('id_pacote_ingresso'),
			'descricao_pacote'	 	=> $this->input->post('descricao_pacote'),
			'quantidade_ingresso' 	=> $this->input->post('quantidade_ingresso'),
			'valor_pacote' 			=> $this->input->post('valor_pacote'),
			'gratis' 				=> $this->input->post('gratis')
		);

		if ($this->form_validation->run()) {

			$this->model_pacotes->start();
			
			$this->model_pacotes->update($dados);

			$commit = $this->model_pacotes->commit();

			if ($commit['status']) {
				$this->aviso('Pacote Criado','Pacote "'.$this->input->post('descricao_pacote').'" editado com sucesso!.','success',false);

				redirect('main/redirecionar/26/'.$dados['id_pacote_ingresso']);
			} else {
				$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/26/'.$dados['id_pacote_ingresso']);
			}


		} else {

			$this->aviso('Falha ao criar','Erro(s) no formulário: '.validation_errors(),'error',true);
			$this->session->set_flashdata($dados);

			redirect('main/redirecionar/26/'.$dados['id_pacote_ingresso']);

		}

	}
	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$aviso_ = str_replace('
', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}

	public function detalhesVendasPacote(){

		$id = $this->input->post('id');

		$detalhes = $this->model_pacotes->detalhes_vendas_pacote($id);


		echo '<table class="table table-bordered table-hover" id="detalhesVendasPacote" align="center">

			    <thead>
			        <tr>
			            <th>Status</th>
			            <th>Detalhes</th>
			            <th>Data aprovação</th>
			            <th>Data Tentativa</th>
			            <th>Última atualização</th>
			        </tr>
			    </thead>
			    <tbody>';

			    foreach ($detalhes as $key => $detalhe) {
			    	echo "<tr>";
		    			echo "<td>{$detalhe->status}</td>";
		    			echo "<td>{$detalhe->status_detail}</td>";
		    			echo "<td>{$detalhe->date_approved}</td>";
		    			echo "<td>{$detalhe->date_created}</td>";
		    			echo "<td>{$detalhe->last_modified}</td>";
		    		echo "</tr>";
			    }

	    echo "</tbody>
	</table>";


	}

}