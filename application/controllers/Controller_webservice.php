<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Controller_webservice extends REST_Controller {

    private $aparelho = null;
    private $ip = null;
    private $sistema = null;
    private $sexo = '28,29';
    private $estado = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27';

    function __construct() {

        parent::__construct();
        // Ex. definindo limites, habilitar em application/config/rest.php
        $this->methods['perfil_get']['limit'] = 500; // 500 requisições por hora, usuário ou chave
        $this->methods['users_post']['limit'] = 100; // 100 requisições por hora, usuário ou chave
        $this->load->model('model_webservice');
        $this->load->model('model_usuarios');
        $this->load->model('model_categorias');
        $this->load->model('model_produtos');
        $this->load->model('model_leiloes');
        $this->load->model('model_ingressos');
        $this->load->model('model_pacotes');

        //Resgatando dados do acesso.
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->sistema = $_SERVER['HTTP_USER_AGENT'];
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
        $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");

        if ($iPod || $iPhone || $iPad) {
            $this->aparelho = IOS;
        } else if ($Android) {
            $this->aparelho = ANDROID;
        } else { //Caso abra pelo Navegador de MAC / PC / Linux
            $this->aparelho = WEB;
        }
    }

    ##################################################################
    /**
     * 	Garante que quem está usando o sistema possui credenciais de acesso.
     * 	@param $grupo_permitir ao passar um valor será limitado o acesso para o grupo em questão,
     * 	@param $valores array que rece o email_usuario e senha_usuario (em sha1) para validar o acesso.
     */

    public function autenticacao($grupo_permitir = null, $fbid = null) {

        if (isset($fbid)) { // autenticação se receber login do facebook

            $autenticacao = $this->model_webservice->validar_login_facebook($fbid);
            
            if ($autenticacao) {

                if (isset($grupo_permitir) && $grupo_permitir != $autenticacao->fk_grupo_usuario) {
                    $this->response(array('status' => STATUS_FALHA_PERFIL, 'resultado' => 'Sem permissão para chamar essa função'), Self::HTTP_ACCEPTED); //202
                } else if (!$autenticacao->ativo_usuario) {
                    $this->response(array('status' => STATUS_FALHA_PERFIL_INATIVO, 'resultado' => 'Perfil Inativo!'), Self::HTTP_ACCEPTED); //202
                } else if (!$autenticacao->ativado_sms) {
                    $this->response(array('status' => STATUS_FALHA_SMS, 'celular_usuario' => $autenticacao->celular_usuario, 'id_usuario' => $autenticacao->id_usuario, 'resultado' => 'Telefone não validado!'), Self::HTTP_ACCEPTED); //202
                } else if ($autenticacao->bloquear) {
                    $this->response(array('status' => STATUS_COMPLETAR_PERFIL, 'resultado' => 'Para prosseguir por favor complete seu perfil', 'usuarios' => $this->model_webservice->preEditarUsuario($autenticacao->id_usuario)), Self::HTTP_ACCEPTED); //202
                } else {
                    return $autenticacao;
                }
            } else {
                $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Dados incorretos!'), Self::HTTP_ACCEPTED); //404
            }

        } else { // autenticação se não receber id do facebook, login e senha

            $PHP_AUTH_USER = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : null;
            $PHP_AUTH_PW = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : null;

            $valores = array(
                'email_usuario' => $PHP_AUTH_USER,
                'senha_usuario' => $PHP_AUTH_PW
            );

            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('email_usuario', 'Login do Usuário', 'required');
            $this->form_validation->set_rules('senha_usuario', 'Senha do Usuário (Criptografada)', 'required');

            if ($this->form_validation->run()) {

                $autenticacao = $this->model_webservice->validar_login($valores);

                if ($autenticacao) {

                    if (isset($grupo_permitir) && $grupo_permitir != $autenticacao->fk_grupo_usuario) {
                        $this->response(array('status' => STATUS_FALHA_PERFIL, 'resultado' => 'Sem permissão para chamar essa função'), Self::HTTP_ACCEPTED); //404
                    } else if (!$autenticacao->ativo_usuario) {
                        $this->response(array('status' => STATUS_FALHA_PERFIL_INATIVO, 'resultado' => 'Perfil Inativo!'), Self::HTTP_ACCEPTED); //404
                    } else if (!$autenticacao->ativado_sms) {
                        $this->response(array('status' => STATUS_FALHA_SMS, 'celular_usuario' => $autenticacao->celular_usuario, 'id_usuario' => $autenticacao->id_usuario, 'resultado' => 'Telefone não validado!'), Self::HTTP_ACCEPTED); //404
                    } else if ($autenticacao->bloquear) {
                        $this->response(array('status' => STATUS_COMPLETAR_PERFIL, 'resultado' => 'Para prosseguir por favor complete seu perfil', 'usuarios' => $this->model_webservice->preEditarUsuario($autenticacao->id_usuario)), Self::HTTP_ACCEPTED); //202
                    } else {
                        return $autenticacao;
                    }
                } else {
                    $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Dados incorretos!'), Self::HTTP_UNAUTHORIZED); //404
                }
            } else { //Campos em branco.				
                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n", "", $erros), "erros" => $this->form_validation->error_array()), Self::HTTP_BAD_REQUEST); //400
            }
        }
    }

    ##################################################################
    /**
     *  Garante que quem está usando o sistema possui credenciais de acesso.
     *  @param 
     */

    public function autenticacao_token($perfil = null) {
        
        $token = $this->getAuthorizationHeader();

        if (!is_null($token)) {
            
            $autenticacao = $this->model_webservice->validar_token($token);

            if ($autenticacao) {

                if (isset($grupo_permitir) && $grupo_permitir != $autenticacao->fk_grupo_usuario) {
                    $this->response(array('status' => STATUS_FALHA_PERFIL, 'resultado' => 'Sem permissão para chamar essa função'), Self::HTTP_ACCEPTED); //404
                } else if (!$autenticacao->ativo_usuario) {
                    $this->response(array('status' => STATUS_FALHA_PERFIL_INATIVO, 'resultado' => 'Perfil Inativo!'), Self::HTTP_ACCEPTED); //404
                } else if (!$autenticacao->ativado_sms) {
                    $this->response(array('status' => STATUS_FALHA_SMS, 'celular_usuario' => $autenticacao->celular_usuario, 'id_usuario' => $autenticacao->id_usuario, 'resultado' => 'Telefone não validado!'), Self::HTTP_ACCEPTED); //404
                } else if ($autenticacao->bloquear && is_null($perfil)) {
                    $this->response(array('status' => STATUS_COMPLETAR_PERFIL, 'resultado' => 'Para prosseguir por favor complete seu perfil', 'usuarios' => $this->model_webservice->preEditarUsuario($autenticacao->id_usuario)), Self::HTTP_ACCEPTED); //202
                } else {
                    $this->forcePayment($autenticacao->id_usuario);
                    return $autenticacao;
                }
            } else {
                $this->response(array('status' => STATUS_TOKEN_INVALIDO, 'resultado' => 'Token inválido!'), Self::HTTP_UNAUTHORIZED); //404
            }
        } else { //Campos em branco.
            $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Não autenticado'), Self::HTTP_BAD_REQUEST); //400
        }
    }

    public function forcePayment($id_usuario){

        $curl = curl_init();
        $address = $_SERVER['SERVER_NAME'].base_url();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://{$address}controller_pagamentos/forcarMultiploEnvio?id_usuario=".$id_usuario,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

    }

    ##################################################################
    /**
     * 	WS1 Recuperar Senha
     * 	@param $valores array que recebe o email_usuario para localizar o E-mail e criar uma nova senha
     */

    public function esqueci_senha_get() {

        $valores = array('email_usuario' => $this->get('email_usuario'));

        $this->form_validation->set_data($valores);
        $this->form_validation->set_rules('email_usuario', 'E-mail do Usuário', 'required');

        if ($this->form_validation->run()) {

            $this->load->helper('string');
            $senha = random_string('numeric', 6);

            $usuario = $this->model_usuarios->senha_Email(sha1($senha), $this->get('email_usuario'));

            $email       = $usuario['email'];
            $nomeUsuario = $usuario['nome'];

            //Usuário inativo ou desativado
            if ($email == "") {
                
                $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Dados incorretos ou perfil inativo!'), Self::HTTP_ACCEPTED); //404

            } else { //Senha enviada para o E-mail.
                
                $emailCorpo = '
                    <p>Sua nova senha de acesso ao leilão 24H é:</p>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                    <tbody>
                        <tr>
                        <td align="left">
                            <h1>'.$senha.'</h1>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                    ';
                
                if ($this->bodyEmail('Nova Senha',$emailCorpo,$email,$nomeUsuario)) {       
                    $this->response(array('status' => STATUS_OK, 'resultado' => "Nova Senha enviada para o E-mail: (" . $email . ")"), Self::HTTP_OK); //200
                } else {
                    $this->response(array('status' => STATUS_FALHA, 'resultado' => "Erro ao enviar senha: "), Self::HTTP_BAD_REQUEST); //400
                }

            }
        } else { //Campos Preenchidos
            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n", "", $erros), "erros" => $this->form_validation->error_array()), Self::HTTP_BAD_REQUEST); //400
        }
    }

    ##################################################################
    /**
     * 	WS2 Login
     * 	@param $autenticacao array que recebe os dados de acesso do usuário
     */

    public function login_usuario_post() {

        $fbid   = $this->post('facebook_usuario');
        $tipo   = $this->aparelho;
        $token  = $this->post('token'); //Push

        $autenticacao = $this->autenticacao(null, isset($fbid) ? $fbid : null);
        if ($autenticacao) {

            if(isset($token) && $token != ""){
                $this->model_webservice->atualizarInserirToken($autenticacao->id_usuario,$tipo,$token);
            }

            // $this->response(array('status' => STATUS_OK, 'resultado' => "Login realizado com sucesso", 'dados' => $autenticacao),Self::HTTP_OK); //200

            $pendencias = $this->model_webservice->getPendencias($autenticacao->id_usuario);

            if(count($pendencias) > 1){
                $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Login realizado com sucesso', array('dados' => array('id_usuario' => $autenticacao->id_usuario), 'pendencias' => $pendencias));
            } else {
                $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Login realizado com sucesso', array('dados' => $autenticacao,'pendencias' => $pendencias));
            }
        }
    }

    ######################################################
    //WS2.1 Ativação SMS
    ######################################################
    public function ativar_sms_post(){

        if($this->model_webservice->ativarSms($this->post('id_usuario'))) {

            $this->response(array(
                'status' => STATUS_OK,
                'resultado' => "Ativação realizada com sucesso."),Self::HTTP_OK); //200

        } else {

            $this->response(array(
                'status' => STATUS_FALHA,
                'resultado' => "Ativação falhou."),Self::HTTP_OK); //200

        }

    }

    ##################################################################
    /**
     * 	WS3 Criar Usuário
     * 	@param $valores array que recebe os dados de acesso do usuário a ser criado
     */

    public function criar_usuario_post() {

        $indicacao = $this->post('code');

        $valores = array(
            'nome_usuario'      => $this->post('nome_usuario'),
            'login_usuario'     => $this->post('email_usuario'),
            'email_usuario'     => $this->post('email_usuario'),
            'celular_usuario'   => preg_replace("/[^0-9]/", "", $this->post('celular_usuario')),
            'senha_usuario'     => $this->post('senha_usuario'),
            'fbid'              => $this->post('facebook_usuario'),
            'fk_grupo_usuario'  => 3
        );

        if(isset($indicacao) && $indicacao != ""){
            $valores['usuario_criou_usuario'] = base64_decode($indicacao);
        }

        $this->form_validation->set_data($valores);
        // $this->form_validation->set_rules('login_usuario','Login do Usuário',				 'required');
        $this->form_validation->set_rules('nome_usuario',       'Nome do Usuário',                  'required');
        $this->form_validation->set_rules('celular_usuario',    'Celular do usuário',               'required|is_unique[seg_usuarios.celular_usuario]|min_length[11]|max_length[11]');
        $this->form_validation->set_rules('email_usuario',      'Email do Usuário',                 'required|is_unique[seg_usuarios.email_usuario]');
        $this->form_validation->set_rules('usuario_criou_usuario', 'CODE', 'callback_usuario_criou_usuario');
        $this->form_validation->set_rules('senha_usuario',      'Senha do Usuário (Criptografada)', 'required');

        if ($this->form_validation->run()) {

            $this->model_webservice->start();
            $this->model_webservice->criarUsuario($valores);

            if(isset($valores['usuario_criou_usuario'])){

                $this->model_webservice->inserirCredito($valores['usuario_criou_usuario']);

            }

            $commit = $this->model_webservice->commit();

            if ($commit['status']) {

                $emailCorpo = '
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                    <tbody>
                        <tr>
                        <td align="left">
                            <p>Obrigado por se cadastrar em nosso sistema! agora você pode arrematar e leiloar seus produtos.</p>
                            <p>Experimente agora mesmo!</p>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                    ';
                
                $this->bodyEmail('Bem-vindo(a)!',$emailCorpo,$valores['email_usuario'],$valores['nome_usuario']);
                $this->response(array('status' => STATUS_OK, 'resultado' => "Perfil criado com sucesso."), Self::HTTP_OK);          //200	

            } else {

                $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha {$commit['message']}"), Self::HTTP_ACCEPTED); //202
            }

        } else { //Campos Preenchidos

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n", "", $erros), "erros" => $this->form_validation->error_array()), Self::HTTP_ACCEPTED);  //202

        }

    }

    public function usuario_criou_usuario($code){

        if ($code != "") {

            $consulta = $this->model_webservice->validarNumCode($code);

            if (isset($consulta) && $consulta->num_rows() == 1) {
                return TRUE;
            } else {
                $this->form_validation->set_message('usuario_criou_usuario', 'Código Compartilhado é inválido');
                return FALSE;
            }

        } else {
            return TRUE;
        }

    }

    ##################################################################
    /**
     * 	WS3.1 Consulta comprar
     */

    public function pendencias_get() {
        $autenticacao = $this->autenticacao_token();
        if ($autenticacao) {
            $pendencias = $this->model_webservice->getPendencias($autenticacao->id_usuario);
            $this->response(array('status' => STATUS_OK, 'resultado' => "Lista de pendencias", 'pendencias' => $pendencias), Self::HTTP_OK); //200
        }
    }

    ##################################################################
    /**
     * 	WS4 Pré editar Usuário
     */

    public function pre_editar_usuario_get() {
        $autenticacao = $this->autenticacao_token(1);
        if ($autenticacao) {
            $this->response(array('status' => STATUS_OK, 'resultado' => "Pré editar usuários", 'usuarios' => $this->model_webservice->preEditarUsuario($autenticacao->id_usuario)), Self::HTTP_OK); //200
        }
    }

    ##################################################################
    /**
     * 	WS5 Editar Usuário
     * 	@param $autenticacao array que recebe os dados de acesso do usuário
     */

    public function editar_usuario_put() {

        $id_usuario = $this->put('id_usuario');

        $autenticacao = $this->autenticacao_token(1);
        if (isset($id_usuario) || $autenticacao) {

            if(!isset($id_usuario)){
                $id_usuario = $autenticacao->id_usuario;
            }

            $valores = array(
                'id_usuario'          => $id_usuario,
                'nome_usuario'        => $this->put('nome_usuario'),
                'sexo_usuario'        => $this->put('sexo_usuario'),
                'rg_usuario'          => preg_replace("/[^0-9a-zA-Z]/", "", $this->put('rg_usuario')),
                'cpf_usuario'         => preg_replace("/[^0-9]/", "", $this->put('cpf_usuario')),
                'telefone_usuario'    => preg_replace("/[^0-9]/", "", $this->put('telefone_usuario')),
                'celular_usuario'     => preg_replace("/[^0-9]/", "", $this->put('celular_usuario')),
                'cep_usuario'         => preg_replace("/[^0-9]/", "", $this->put('cep_usuario')),
                'logradouro_usuario'  => $this->put('logradouro_usuario'),
                'numero_usuario'      => $this->put('numero_usuario'),
                'bairro_usuario'      => $this->put('bairro_usuario'),
                'cidade_usuario'      => $this->put('cidade_usuario'),
                'estado_usuario'      => $this->put('estado_usuario'),
                'complemento_usuario' => $this->put('complemento_usuario')
            );

            $senha = trim($this->put('senha_usuario'));

            if (isset($senha) && $senha != '') {
                $valores['senha_usuario'] = $senha;
            }

            $email = $this->put('email_usuario');

            if (isset($email) && $email != '') {
                $valores['email_usuario'] = $email;
                $valores['login_usuario'] = $email;
            }

            if (isset($autenticacao->celular_usuario) && $valores['celular_usuario'] != $autenticacao->celular_usuario) {
                $valores['ativado_sms'] = 0;
            }

            $this->form_validation->set_data($valores);
            $this->form_validation->set_rules('nome_usuario', 'Nome do usuário', 'required');
            $this->form_validation->set_rules('sexo_usuario', 'Sexo do usuário', 'in_list['.$this->sexo.']');
            $this->form_validation->set_rules('rg_usuario', 'RG do usuário', 'max_length[11]');
            $this->form_validation->set_rules('cpf_usuario', 'CPF do usuário', 'max_length[11]|edit_unique[seg_usuarios.cpf_usuario.'.$id_usuario.']');
            $this->form_validation->set_rules('celular_usuario', 'Celular do usuário', 'required|min_length[11]|max_length[11]');
            $this->form_validation->set_rules('telefone_usuario', 'Telefone do usuário', 'min_length[10]|max_length[11]');
            // $this->form_validation->set_rules('login_usuario', 'Login do Usuário', 'required');
            $this->form_validation->set_rules('email_usuario', 'Email do Usuário', 'valid_email|edit_unique[seg_usuarios.email_usuario.'.$id_usuario.']');
            $this->form_validation->set_rules('cep_usuario', 'CEP do usuário', 'min_length[8]|max_length[8]');
            $this->form_validation->set_rules('estado_usuario', 'Estado do usuário', 'in_list['.$this->estado.']');

            if ($this->form_validation->run()) {

                $this->model_webservice->start();
                $this->model_webservice->editarUsuario($valores);
                $commit = $this->model_webservice->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Perfil {$valores['nome_usuario']} editado com sucesso."), Self::HTTP_OK); //200	  				
                } else {

                    $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha {$commit['message']}"), Self::HTTP_ACCEPTED); //404
                }
            } else { //Campos Preenchidos
                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n", "", $erros), "erros" => $this->form_validation->error_array()), Self::HTTP_ACCEPTED); //400
            }
        }
    } 


    ##################################################################
    /**
     * 	WS6 Pré criar produto
     * 	@param $autenticacao array que recebe os dados de acesso do usuário
     */

    public function pre_criar_produto_get() {

        $autenticacao = $this->autenticacao_token();

        $id_categoria = $this->input->get('id_categoria');

        if ($autenticacao) {
            $categorias = $this->model_categorias->listaCategorias($id_categoria);
            $this->response(array('status' => STATUS_OK, 'resultado' => "Pré criar produto", 'categorias' => $categorias), Self::HTTP_OK); //200	
        }
    }

    ##################################################################
    /**
     * 	WS7 Criar produto
     * 	@param $autenticacao array que recebe os dados de acesso do usuário
     */

    public function criar_produto_post() {

        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {

            $dados = array(
                'fk_categoria'      => $this->post('fk_categoria'),
                'nome_produto'      => $this->post('nome_produto'),
                'descricao_produto' => $this->post('descricao_produto'),
                'produto_usado'     => $this->post('produto_usado'),
                'fk_usuario'        => $autenticacao->id_usuario
            );

            $imagens = $this->post('imagem');

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('fk_categoria', 'ID da categoria', 'required|integer');
            $this->form_validation->set_rules('nome_produto', 'Nome do produto', 'required');
            $this->form_validation->set_rules('produto_usado', 'Usado/Novo', 'required');

            if ($this->form_validation->run()) {

                $this->model_produtos->start();
                $id_produto = $this->model_produtos->criarProduto($dados);

                if (!$this->base64ToImageProduto($id_produto, $imagens)) {
                    $this->model_produtos->rollback();
                }

                $commit = $this->model_produtos->commit();

                if ($commit['status']) {
                    $this->response(array('status' => STATUS_OK, 'resultado' => "Produto criado com sucesso.", 'id_produto' => $id_produto),Self::HTTP_OK); //200
                } else {

                    $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha {$commit['message']}"), Self::HTTP_ACCEPTED); //404
                }
            } else {
                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n", "", $erros), "erros" => $this->form_validation->error_array()), Self::HTTP_BAD_REQUEST); //400
            }
        }
    }

    ##################################################################
    /**
     * 	WS7.1 Editar produto
     * 	@param $autenticacao array que recebe os dados de acesso do usuário
     */

    public function editar_produto_post() {

        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {

            $dados = array(
                'id_produto'        => $this->post('id_produto'),
                'fk_categoria'      => $this->post('fk_categoria'),
                'nome_produto'      => $this->post('nome_produto'),
                'descricao_produto' => $this->post('descricao_produto'),
                'produto_usado'     => $this->post('produto_usado')
            );

            $imagens = $this->post('imagem');

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('id_produto',     'ID da Produto', 'required|integer');
            $this->form_validation->set_rules('fk_categoria',   'ID da categoria', 'required|integer');
            $this->form_validation->set_rules('nome_produto',   'Nome do produto', 'required');
            $this->form_validation->set_rules('produto_usado',  'Usado/Novo', 'required');

            if ($this->form_validation->run()) {

                $this->model_produtos->start();
                $this->model_produtos->update($dados);
                $id_produto = $dados['id_produto'];

                if (!$this->base64ToImageProduto($id_produto, $imagens)) {
                    $this->model_produtos->rollback();
                }

                $commit = $this->model_produtos->commit();

                if ($commit['status']) {
                    $this->response(array('status' => STATUS_OK, 'resultado' => "Produto editado com sucesso.", 'id_produto' => $id_produto),Self::HTTP_OK); //200
                } else {

                    $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha {$commit['message']}"), Self::HTTP_ACCEPTED); //404
                }
            } else {
                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n", "", $erros), "erros" => $this->form_validation->error_array()), Self::HTTP_BAD_REQUEST); //400
            }
        }
    }

    ##################################################################
    /**
     * 	WS8 Listar produto
     * 	@param $autenticacao array que recebe os dados de acesso do usuário
     */

    public function listar_produtos_get() {

        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {

            $produtos = $this->model_produtos->listarProduto($autenticacao->id_usuario);

            foreach ($produtos as $key => $produto) {
                $imagens = $this->listar_imagens($produto['id_produto']);
                $produtos[$key]['imagens'] = $imagens;
            }

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de produtos', array('produtos' => $produtos));
        }
    }

    ##################################################################
    /**
     * 	WS9 Criar leilão
     * 	@param $autenticacao array que recebe os dados de acesso do usuário
     * 	@param $fk_produto
     * 	@param $valor_minimo
     * 	@param $altura
     * 	@param $largura
     * 	@param $comprimento
     * 	@param $peso
     */

    public function criar_leilao_post() {

        $autenticacao = $this->autenticacao_token();

        $altura         = $this->post('altura');
        $largura        = $this->post('largura');
        $comprimento    = $this->post('comprimento');
        $peso           = $this->post('peso');

        if ($autenticacao) {

            $dados = array(
                'fk_produto'        => $this->post('id_produto'),
                'valor_minimo'      => $this->post('valor_minimo'),
                'valor_inicial_lance'      => $this->post('valor_minimo'),
                'altura'            => isset($altura)       ? $altura       : 0,
                'largura'           => isset($largura)      ? $largura      : 0,
                'comprimento'       => isset($comprimento)  ? $comprimento  : 0,
                'peso'              => isset($peso)         ? $peso         : 0,
                'data_inicio'       => date('Y-m-d H:i:s'),
                'data_fim_previsto' => date('Y-m-d H:i:s'),
                'status_leilao'     => 1
            );

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('fk_produto',     'ID Produto',   'required|integer');
            $this->form_validation->set_rules('valor_minimo',   'Valor mínimo', 'required');
            // $this->form_validation->set_rules('altura',         'Altura',       'required');
            // $this->form_validation->set_rules('largura',        'Largura',      'required');
            // $this->form_validation->set_rules('comprimento',    'Comprimento',  'required');
            // $this->form_validation->set_rules('peso',           'Peso',         'required');

            if ($this->form_validation->run()) {
                $this->model_leiloes->start();
                $id_leilao = $this->model_leiloes->insert($dados);
                $commit = $this->model_leiloes->commit();

                if ($commit['status']) {
                    $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Leilão cadastro com sucesso', array('id_leilao' => $id_leilao));
                } else {
                    $this->response(array('status' => STATUS_FALHA, 'resultado' => $commit), Self::HTTP_ACCEPTED); //404
                }
                
            } else {
                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n", "", $erros), "erros" => $this->form_validation->error_array()), Self::HTTP_BAD_REQUEST); //400
            }
        }
    }

    function validaProdutoUsuario($id_produto) {

        $autenticacao = $this->autenticacao_token();

        $produto = $this->model_produtos->buscarProduto($id_produto);

        if ($autenticacao->id_usuario <> $produto->id_produto) {
            $this->form_validation->set_message('Produto', 'Usuário e produto incorretos');
            return false;
        }

        return true;
    }

    ##################################################################
    /**
     * 	WS10 Listar leilões
     * 	@param $autenticacao array que recebe os dados de acesso do usuário (Opcional agora)
     *  @param $quantidade_por_pagina quantidade resultados por pagina
     *  @param $pagina página do resultado
     */

    public function listar_leiloes_ativos_get() {

        $qtd_pg = $this->input->get('quantidade_por_pagina');
        $pagina = $this->input->get('pagina');
        $filtro = $this->input->get('id_leilao');

        if ($this->getAuthorizationHeader() == "free") {
            $this->listar_leiloes_ativos(0,$qtd_pg,$pagina,$filtro);
        } else {
            $autenticacao = $this->autenticacao_token();
            if($autenticacao){
                $this->listar_leiloes_ativos($autenticacao->id_usuario,$qtd_pg,$pagina,$filtro);
            }
        }
        
    }

    public function listar_leiloes_ativos($id,$qtd_pg,$pagina,$filtro) {

        if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
            $qtd_pg = 10;
        }
        
        if (is_null($pagina) || $pagina == '' || $pagina == 0) {
            $pagina = 1;
        }

        $dados = array(
            'id_usuario' => $id,
            'limit' => $qtd_pg,
            'offset' => (($qtd_pg * $pagina) - $qtd_pg),
            'pagina' => $pagina
        );

        if (!is_null($filtro)){
            $dados['id_leilao'] = $filtro ;
        }

        $leiloes = $this->model_leiloes->getLeiloes($dados);

        foreach ($leiloes as $key => $leilao) {
            $imagens = $this->listar_imagens($leilao['id_produto']);
            $leiloes[$key]['imagens'] = $imagens;
        }

        $this->resposta($id, STATUS_OK, 'Lista de leilões', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'leiloes' => $leiloes));

    }

    ##################################################################
    /**
     * 	WS11 Entrar no leilão
     * 	@param $autenticacao array que recebe os dados de acesso do usuário
     */

    public function entrar_leilao_post() {

        $id_leilao = $this->input->post('id_leilao');
        $id_usuario = 1;

        if ($this->getAuthorizationHeader() == "free") {
            $this->resposta($id_usuario, STATUS_OK, 'Usuário sem token');
        } else {
            $autenticacao = $this->autenticacao_token();
            if ($autenticacao) {
                $id_usuario = $autenticacao->id_usuario;
                $sala = $this->model_leiloes->consultaSala(array('fk_leilao' => $id_leilao, 'fk_usuario' => $id_usuario));
                if ($sala->num_rows() > 0) {
                    $this->resposta($id_usuario, STATUS_OK, 'Usuário está na sala');                
                } else {                    
                    $this->cobrar_ingresso($id_usuario, $id_leilao);
                }
            }
        }
    }

    ##################################################################
    /**
     * 	Cobra ingresso para entrar na sala
     * 	@param $id_usuario
     * 	@param $id_leilao
     */

    function cobrar_ingresso($id_usuario, $id_leilao) {

        $this->model_ingressos->start();

        if (!$this->model_ingressos->cobrarIngresso($id_usuario, $id_leilao)) {
            $this->resposta($id_usuario, STATUS_COMPRAR_INGRESSOS, 'Ingressos insuficientes para entrar na sala');
        } else {

            $atingido = $this->model_ingressos->entrarSala($id_usuario, $id_leilao);
            /*Verificar o número de pessoas na sala, caso atinja o mínimo solicitado será enviado PUSH para todos, a data fim será alterada e o cronometro iniciado.*/
            if($atingido->atingido){

                //Enviar Push
                $curl = curl_init();

                $usuarios   = $this->model_ingressos->listarUsuarios($id_leilao);
                $titulo     = "O leilão do(a) '{$atingido->nome_produto}' começou!";
                $mensagem   = "A contagem regressiva de {$atingido->tempo_iniciar} minutos começa agora! Deixe seu lance.";

                $address = $_SERVER['SERVER_NAME'].base_url();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "http://{$address}controller_notificacoes/notificando?usuarios=".urlencode($usuarios)."&titulo=".urlencode($titulo)."&mensagem=".urlencode($mensagem)."&id_leilao={$id_leilao}",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "GET",
                  CURLOPT_HTTPHEADER => array("Cache-Control: no-cache"),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                $this->model_ingressos->novoTempoFim($id_leilao);

            }

            $commit = $this->model_ingressos->commit();

            if ($commit['status']) {
                $this->resposta($id_usuario, STATUS_OK, 'Cobrança Realizada com sucesso');
            } else {
                $this->resposta($id_usuario, STATUS_FALHA, 'Falha ao entrar na sala.');
            }
        }

        //fazer função para debitar ingresso e insert na tabela sala_leilao
    }

    ##################################################################
    /**
     *  WS12 Lista pacotes de ingressos para serem comprados
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     *  @param $quantidade_por_pagina quantidade resultados por pagina
     *  @param $pagina página do resultado
     */
    public function listar_pacotes_get() {
        
        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {

            $qtd_pg = $this->input->get('quantidade_por_pagina');
            $pagina = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'id_usuario' => $autenticacao->id_usuario,
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $ingressos = $this->model_ingressos->listarPacotes($dados);

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de pacotes', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'pacotes' => $ingressos));
        }

    }


    ##################################################################
    /**
     *  WS13 Insere ou remover leilão na lista de favoritos do usuário
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     *  @param $id_leilao ID do leilão
     */
    public function favorito_post() {
        
        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {
            
            $id_leilao = $this->input->post('id_leilao');

            $resposta = $this->model_leiloes->favorito($autenticacao->id_usuario, $id_leilao);

            $this->resposta($autenticacao->id_usuario, STATUS_OK, $resposta);
        }
    }


    ##################################################################
    /**
     *  WS14 Listar favoritos
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     */
    public function listar_favoritos_get() {
        
        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {
            
            $qtd_pg = $this->input->get('quantidade_por_pagina');
            $pagina = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'id_usuario' => $autenticacao->id_usuario,
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $leiloes = $this->model_leiloes->listarFavoritos($dados);

            foreach ($leiloes as $key => $leilao) {
                $imagens = $this->listar_imagens($leilao['id_produto']);
                $leiloes[$key]['imagens'] = $imagens;
            }

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de leilões favoritos', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'leiloes' => $leiloes));
        }
    }


    ##################################################################
    /**
     *  WS15 Meus leilões
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     *  @param $quantidade_por_pagina quantidade resultados por pagina
     *  @param $pagina página do resultado
     */

    public function meus_leiloes_get() {

        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {

            $qtd_pg = $this->input->get('quantidade_por_pagina');
            $pagina = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'id_usuario' => $autenticacao->id_usuario,
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $leiloes = $this->model_leiloes->meusLeiloes($dados);

            foreach ($leiloes as $key => $leilao) {
                $imagens = $this->listar_imagens($leilao['id_produto']);
                $leiloes[$key]['imagens']       = $imagens;
                //$leiloes[$key]['ultimosLances'] = $this->model_leiloes->ultimosLances($leilao['id_leilao']);
            }

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de leilões', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'leiloes' => $leiloes));
        }
    }

    ##################################################################
    /**
     *  WS16 Upload foto perfil
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     *  @param $foto_perfil imagem do perfil em base64
     */
    public function upload_foto_post() {
        
        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {

            $imgs = array('foto' => $this->post('foto'));

            $this->form_validation->set_data($imgs);
            $this->form_validation->set_rules('foto', 'foto', 'required');

            if ($this->form_validation->run()) {
                
                if ($this->base64ToImage($autenticacao->id_usuario, $imgs, '', 'perfil')) {

                    $this->response(array('status' => STATUS_OK, 'resultado' => "Imagem armazenada com sucesso"),Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Erro ao armazenar imagem'),Self::HTTP_OK); //202

                }               
            } else {
                
                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\n","",$erros), "erros" => $this->form_validation->error_array()),Self::HTTP_OK); //202

            }
        }
    }

    ##################################################################
    /**
     *  WS17 Meus leilões
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     *  @param $quantidade_por_pagina quantidade resultados por pagina
     *  @param $pagina página do resultado
     */

    public function leiloes_categorias_get() {
        
        if ($this->getAuthorizationHeader() == "free") {
            $id = 1;    
        } else {
            $autenticacao = $this->autenticacao_token();
            if ($autenticacao) {
                $id = $autenticacao->id_usuario;
            }
        }

        $categoria = $this->input->get('categoria');
        $qtd_pg    = $this->input->get('quantidade_por_pagina');
        $pagina    = $this->input->get('pagina');

        if (empty($categoria) || $categoria == '' || $categoria == 0) {
            $categoria = null;
        }

        if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
            $qtd_pg = 10;
        }

        if (is_null($pagina) || $pagina == '' || $pagina == 0) {
            $pagina = 1;
        }

        $dados = array(
            'id_categoria' => $categoria,
            'id_usuario' => $id,
            'limit' => $qtd_pg,
            'offset' => (($qtd_pg * $pagina) - $qtd_pg),
            'pagina' => $pagina
        );

        $leiloes = $this->model_leiloes->getLeiloesCategorias($dados);

        foreach ($leiloes as $key => $leilao) {
            $imagens = $this->listar_imagens($leilao['id_produto']);
            $leiloes[$key]['imagens'] = $imagens;
        }

        $categorias = $this->model_categorias->listaCategorias($categoria);

        $this->resposta($id, STATUS_OK, 'Lista de leilões', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'leiloes' => $leiloes, 'categorias' => $categorias));

    }

    ##################################################################
    /**
     *  WS18 Buscar leilões
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     *  @param $quantidade_por_pagina quantidade resultados por pagina
     *  @param $pagina página do resultado
     */

    public function buscar_leiloes_get() {

        if ($this->getAuthorizationHeader() == "free") {
            $id = 1;    
        } else {
            $autenticacao = $this->autenticacao_token();
            if ($autenticacao) {
                $id = $autenticacao->id_usuario;
            }
        }

        $busca = $this->input->get('busca');
        $qtd_pg    = $this->input->get('quantidade_por_pagina');
        $pagina    = $this->input->get('pagina');

        if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
            $qtd_pg = 10;
        }

        if (is_null($pagina) || $pagina == '' || $pagina == 0) {
            $pagina = 1;
        }

        $dados = array(
            'busca' => $busca,
            'id_usuario' => $id,
            'limit' => $qtd_pg,
            'offset' => (($qtd_pg * $pagina) - $qtd_pg),
            'pagina' => $pagina
        );

        $this->form_validation->set_data($dados);
        $this->form_validation->set_rules('busca', 'Campo de busca', 'required');

        if ($this->form_validation->run()) {
                $leiloes = $this->model_leiloes->buscarLeiloes($dados);

            foreach ($leiloes as $key => $leilao) {
                $imagens = $this->listar_imagens($leilao['id_produto']);
                $leiloes[$key]['imagens'] = $imagens;
            }

            $this->resposta($id, STATUS_OK, 'Lista de leilões', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'leiloes' => $leiloes));

        } else {

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\n","",$erros), "erros" => $this->form_validation->error_array()),Self::HTTP_OK); //202
        }
        
    }


    ##################################################################
    /**
     *  Cobra ingresso para entrar na sala
     *  @param $id_usuario
     *  @param $id_recompensas
     */
    public function comprar_ingresso_post() {
        # code...
    }

     ##################################################################
    /**
     *  WS19 Histórico de lances  
     */

    public function historico_lances_get() {
        $autenticacao = $this->autenticacao_token();
         if ($autenticacao) {

            $busca      = $this->input->get('busca');
            $qtd_pg     = $this->input->get('quantidade_por_pagina');
            $pagina     = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'nome_produto' => $busca,
                'id_usuario' => $autenticacao->id_usuario,
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('pagina',                 'Páginas',  'numeric');
            $this->form_validation->set_rules('quantidade_por_pagina',  'Quatidade','numeric');

            if ($this->form_validation->run()) {
                 
                $lances = $this->model_leiloes->historicoLances($dados);
                $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de Lances', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'lances' => $lances));

            } else {

                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\n","",$erros), "erros" => $this->form_validation->error_array()),Self::HTTP_OK); //202
            }

        }

    }


    ##################################################################
    /**
     *  WS20 Avaliar leiloeiro
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     */
    public function avaliar_post() {
        
        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {

            $dados = array('id_leilao' => $this->post('id_leilao'),'id_usuario' => $autenticacao->id_usuario,'avaliacao' => $this->post('avaliacao'));

            $this->form_validation->set_data($dados);
            $this->form_validation->set_rules('id_leilao', 'ID do leilão',  'required|is_unique[cad_avaliacoes.fk_leilao]');
            $this->form_validation->set_rules('avaliacao', 'Avaliação',     'required');

            if ($this->form_validation->run()) {
                
                $this->model_leiloes->start();
                $this->model_leiloes->avaliar($dados);

                $commit = $this->model_leiloes->commit();

                if ($commit['status']) {

                    $this->response(array('status' => STATUS_OK,    'resultado' => 'Avaliação recebida com sucesso!'), Self::HTTP_OK); //200

                } else {

                    $this->response(array('status' => STATUS_FALHA, 'resultado' => "Falha {$commit['message']}"), Self::HTTP_ACCEPTED); //202

                }

            } else {
                
                $erros = strip_tags(validation_errors());
                $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\n","",$erros), "erros" => $this->form_validation->error_array()),Self::HTTP_OK); //202

            }

        }

    }

    ##################################################################
    /**
     *  WS21 Listar Arrematados
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     */
    public function listar_arrematados_get() {
        
        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {
            
            $qtd_pg = $this->input->get('quantidade_por_pagina');
            $pagina = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'id_usuario' => $autenticacao->id_usuario,
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $leiloes = $this->model_leiloes->listarArrematados($dados);

            foreach ($leiloes as $key => $leilao) {
                $imagens = $this->listar_imagens($leilao['id_produto']);
                $leiloes[$key]['imagens'] = $imagens;
            }

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de leilões arrematados', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'leiloes' => $leiloes));
        }
    }

    ##################################################################
    /**
     *  WS22 Listar Afiliados
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     */
    public function listar_afiliados_get() {
        
        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {
            
            $qtd_pg = $this->input->get('quantidade_por_pagina');
            $pagina = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'id_usuario' => $autenticacao->id_usuario,
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $afiliados = $this->model_leiloes->listarAfiliados($dados);

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de afiliados', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'afiliados' => $afiliados, 'pontos_afiliados' => $this->model_leiloes->getCreditos($autenticacao->id_usuario)));

        }
    }

    ##################################################################
    /**
     *  WS23 Listar recompensas
     *  @param $autenticacao array que recebe os dados de acesso do usuário
     */
    public function listar_recompensas_get() {
        
        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {
            
            $qtd_pg = $this->input->get('quantidade_por_pagina');
            $pagina = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $recompensas = $this->model_leiloes->listarRecompensas($dados);

            foreach ($recompensas['produtos'] as $key => $produto) {
                $imagens = $this->listar_imagens($produto['id_produto']);
                $recompensas['produtos'][$key]['imagens'] = $imagens;
            }

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de recompensas', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'recompensas' => $recompensas, 'pontos_afiliados' => $this->model_leiloes->getCreditos($autenticacao->id_usuario)));

        }
    }

    public function listar_minhas_recompensas_get() {
        
        $autenticacao = $this->autenticacao_token();
        if ($autenticacao) {

            $produtos = $this->model_leiloes->listarMinhasRecompensas($autenticacao->id_usuario);
            foreach ($produtos as $key => $produto) {
                $imagens = $this->listar_imagens($produto['id_produto']);
                $produtos[$key]['imagens'] = $imagens;
            }

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista das minhas recompensas', 
                            array('produtos' => $produtos));
        }

    }

    public function comprar_recompensas_post() {

        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {
        
            $id_usuario         = $autenticacao->id_usuario;
            $id_recompensa = $this->post('id_recompensa');
            $tipo_recompensa    = $this->post('tipo_recompensa');

            if($tipo_recompensa == RECOMPENSA_PACOTE){

                $pacote = $this->model_ingressos->getPacote($id_recompensa);
                //Validando Saldo
                if($this->model_leiloes->getCreditos($autenticacao->id_usuario) >= $pacote['valor_pacote']){

                    $cad_ingressos = array(
                        'fk_usuario' => $id_usuario,
                        'quantidade' => $pacote['quantidade_ingresso'],
                        'gratis' => $pacote['gratis']
                    );

                    $compra_ingresso = array(
                        'valor' => $pacote['valor_pacote'],
                        'fk_pacote' => $pacote['id_pacote_ingresso'],
                    );

                    $this->model_ingressos->start();

                    $id_ingresso = $this->model_ingressos->insertIngresso($cad_ingressos,$pacote['valor_pacote']);

                    $compra_ingresso['fk_ingresso'] = $id_ingresso;

                    $id_comprar = $this->model_ingressos->insertCompra($compra_ingresso);

                    $update = array(
                        'id_ingresso' => $id_ingresso,
                        'ativo' => true
                    );

                    $this->model_ingressos->updateIngresso($update);

                    $commit = $this->model_ingressos->commit();

                    if ($commit['status']) {
                        $this->response(array('status' => STATUS_OK,    'resultado' => 'Recompensa adquirida com sucesso','pontos_afiliados' => $this->model_leiloes->getCreditos($autenticacao->id_usuario)),Self::HTTP_OK); //200
                    } else {
                        $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Falha ao adquirir'),Self::HTTP_ACCEPTED); //202
                    }

                } else {
                    $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Pontos insuficientes'),Self::HTTP_ACCEPTED); //202
                }

            } else if ($tipo_recompensa == RECOMPENSA_PRODUTOS) {

                $produto = $this->model_ingressos->getProduto($id_recompensa);
                //Validando Saldo
                if($this->model_leiloes->getCreditos($autenticacao->id_usuario) >= $produto['produto_recompensa_custo']){
                    
                    $this->model_ingressos->start();

                    $this->model_ingressos->debitarCredito($produto['produto_recompensa_custo'],$autenticacao->id_usuario);

                    $dados = array('fk_produto' => $produto['id_produto'], 'fk_usuario' => $autenticacao->id_usuario);
                    $this->model_ingressos->inserirRecompensa($dados);
                    
                    $commit = $this->model_ingressos->commit();

                    if ($commit['status']) {
                        $this->response(array('status' => STATUS_OK,    'resultado' => 'Recompensa adquirida com sucesso','pontos_afiliados' => $this->model_leiloes->getCreditos($autenticacao->id_usuario)),Self::HTTP_OK); //200
                    } else {
                        $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Falha ao adquirir'),Self::HTTP_ACCEPTED); //202
                    }

                } else {
                    $this->response(array('status' => STATUS_FALHA, 'resultado' => 'Pontos insuficientes'),Self::HTTP_ACCEPTED); //202
                }

            }

        }

    }

    public function meus_produtos_arrematados_get(){

        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {
            
            $qtd_pg = $this->input->get('quantidade_por_pagina');
            $pagina = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'id_usuario' => $autenticacao->id_usuario,
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $leiloes = $this->model_leiloes->listarMeusArrematados($dados);

            foreach ($leiloes as $key => $leilao) {
                $imagens = $this->listar_imagens($leilao['id_produto']);
                $leiloes[$key]['imagens'] = $imagens;
            }

            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de leilões arrematados', array('quantidade_por_pagina' => $qtd_pg, 'pagina' => $pagina, 'leiloes' => $leiloes));
        }

    }

    public function listar_meus_pagamentos_get(){

        $autenticacao = $this->autenticacao_token();

        if ($autenticacao) {
            
            $qtd_pg = $this->input->get('quantidade_por_pagina');
            $pagina = $this->input->get('pagina');

            if (is_null($qtd_pg) || $qtd_pg == '' || $qtd_pg == 0) {
                $qtd_pg = 10;
            }

            if (is_null($pagina) || $pagina == '' || $pagina == 0) {
                $pagina = 1;
            }

            $dados = array(
                'id_usuario' => $autenticacao->id_usuario,
                'limit' => $qtd_pg,
                'offset' => (($qtd_pg * $pagina) - $qtd_pg),
                'pagina' => $pagina
            );

            $leiloes = $this->model_pacotes->vendas_pacote($autenticacao->id_usuario);


            $this->resposta($autenticacao->id_usuario, STATUS_OK, 'Lista de pagamentos', $leiloes);
        }

    }






    //Chamado de 1 em 1 minuto para enviar notificações
    public function cronPush_get(){

        $dados = $this->model_webservice->cron_Push();

    }

    ##################################################################
    /**
     * 	Gera resposta, envia saldo de créditos do usuário
     * 	@param $status status da resposta
     * 	@param $resultado resultado da chamada
     * 	@param $array demais campos a serem enviados na resposta, chave é o nome do campo
     */

    function resposta($id_usuario, $status, $resultado, $array = null) {

        if($id_usuario == 0){
            $total_ingressos = 0;    
        } else {
            $total_ingressos = $this->model_ingressos->contaIngressos($id_usuario);
        }

        $response = array(
            'status' => $status,
            'creditos' => $total_ingressos,
            'resultado' => $resultado);

        if (!is_null($array)) {

            foreach ($array as $key => $valor) {
                $response[$key] = $valor;
            }
        }

        $this->response($response, Self::HTTP_OK);
    }

    ##################################################################
    /**
     * 	Busca por CEP
     * 	@param $cep recebe o CEP para buscar o endereço completo
     */

    public function buscar_cep_get() {

        $valores = array('cep' => $this->get('cep'));

        $this->form_validation->set_data($valores);
        $this->form_validation->set_rules('cep', 'CEP', 'required');

        if ($this->form_validation->run()) {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://viacep.com.br/ws/{$valores['cep']}/json/unicode",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Cache-Control: no-cache",
                    "Content-Type: Application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $dado = json_decode($response);
            $dados['cep'] = !isset($dado->cep) ? "" : $dado->cep;
            $dados['logradouro'] = !isset($dado->logradouro) ? "" : $dado->logradouro;
            $dados['complemento'] = !isset($dado->complemento) ? "" : $dado->complemento;
            $dados['bairro'] = !isset($dado->bairro) ? "" : $dado->bairro;
            $dados['localidade'] = !isset($dado->localidade) ? "" : $dado->localidade;
            $dados['uf'] = !isset($dado->uf) ? "" : $dado->uf;
            //Caso queria trazer o ID da UF de uma tabela relacional
            $dados['uf_id']          = !isset($dado->uf) 	   ? "" : $this->model_webservice->filtroUf($dado->uf);

            if ($err) {

                $this->response(array('status' => STATUS_FALHA, 'resultado' => $err), Self::HTTP_BAD_REQUEST); //400
            } else {

                $this->response(array('status' => STATUS_OK, 'resultado' => "Retorno do via cep.", "endereco" => $dados), Self::HTTP_OK); //200
            }

        } else {

            $erros = strip_tags(validation_errors());
            $this->response(array('status' => STATUS_FALHA, 'resultado' => str_replace("\r\n", "", $erros), "erros" => $this->form_validation->error_array()), Self::HTTP_BAD_REQUEST); //400
        }
    }

    ##################################################################
    /**
     * 	Formata a data para o banco de dados
     * 	@param $data 		recebe a data que deseja converter
     * 	@param $timestamp 	recebe um booleano, TRUE caso seja timestamp.
     */

    public function data($data = null, $timestamp = null) {

        if (isset($data) && $data != "" && $timestamp) {

            return date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $data)));
        } else if (isset($data) && $data != "" && !$timestamp) {

            return date("Y-m-d", strtotime(str_replace('/', '-', $data)));
        } else {

            return 0;
        }
    }

    ##################################################################
    /**
     * 	Formata moeda
     * 	@method formatMoeda 		transforma o valor deixando com padrão Brasileiro
     * 	@method formatMoedaBanco 	transforma o valor deixando com padrão para o banco de dados
     * 	@param $valor 				valor a ser convertido
     */

    public function formatMoeda($valor) {
        return 'R$ ' . number_format($valor, 2, ',', '.'); // R$ 1.000,00
    }

    public function formatMoedaBanco($valor) {
        return number_format($valor, 2, '.', ''); // 1000.00
    }

    ##################################################################
    /**
     * 	Remove mascaras, deixando somente números e letras
     * 	@param $valor valor a ser convertido
     */

    public function removerMascaras($valor) {
        return preg_replace('/[^0-9a-zA-Z]/', '', $valor);
    }

    ##################################################################
    /**
     * 	Upload de imagens em Base64 para .PNG e criando uma pasta por usuário. 
     * 	@param $id
     * 	@param $imagem
     */

    function base64ToImageProduto($id, $imagens) {

        $nome = 0;

        if (isset($imagens)) {
            foreach ($imagens as $key => $imageData) {

                if (!(is_null($imageData)) && !($imageData == '')) {

                    $imageData = str_replace("\n", "", $imageData);

                    $imageData = str_replace("\r", "", $imageData);

                    $imageData = str_replace("\\n", "", $imageData);

                    $imageData = str_replace("\\r", "", $imageData);

                    $imageData = str_replace(" ", "+", $imageData);

                    $fileName = $id . '_' . $nome . '.png';
                    $imageData = base64_decode($imageData);
                    $endereco = $_SERVER['DOCUMENT_ROOT'] . base_url() . 'upload/produtos/' . $id;

                    if (!file_exists($endereco)) {
                        mkdir($endereco, 0777, true);
                    }

                    if (file_exists($endereco . '/' . $fileName)) {

                        unlink($endereco . '/' . $fileName);
                    }

                    $nome++;

                    if (file_put_contents($endereco . '/' . $fileName, $imageData) == false) {
                        break;
                        return false;
                    }
                }
            }
        }

        return true;
    }

    ######################################################  
    //Upload de imagens em Base64 para .PNG
    ######################################################
    function base64ToImage($id,$imagens,$nome = '', $dir = null){

        foreach ($imagens as $key => $imageData) {

            if (!(is_null($imageData)) && !($imageData == '')) {
                
                $imageData = str_replace("\n", "", $imageData);

                $imageData = str_replace("\r", "", $imageData);

                $imageData = str_replace("\\n", "", $imageData);

                $imageData = str_replace("\\r", "", $imageData);

                $imageData = str_replace(" ", "+", $imageData);

                $fileName = $id.$nome.'.png';
                $imageData = base64_decode($imageData);

                if (is_null($dir)) {
                    $endereco = $_SERVER['DOCUMENT_ROOT'].base_url().'upload/'.$key;
                } else {
                    $endereco = $_SERVER['DOCUMENT_ROOT'].base_url().'upload/'.$dir.'/'.$key;
                }

                if (!file_exists($endereco)) {
                    mkdir($endereco, 0777, true);
                }

                if(file_exists($endereco.'/'.$fileName)) {

                    unlink($endereco.'/'.$fileName);

                }

                if (file_put_contents($endereco.'/'.$fileName, $imageData) == false) {
                    break;
                    return false;
                }
            }

        }

        return true;
    }

    ##################################################################
    /**
     * 	Listar imagens. 
     * 	@param $id
     */

    function listar_imagens($id = '') {

        $link = base_url() . 'upload/produtos/' . $id;

        $endereco = $_SERVER['DOCUMENT_ROOT'] . $link;

        $imagens = array();

        if (file_exists($endereco)) {

            $types = array('png');

            if ($handle = opendir($endereco)) {
                while ($entry = readdir($handle)) {
                    $ext = strtolower(pathinfo($entry, PATHINFO_EXTENSION));

                    if (in_array($ext, $types)) {
                        $imagens[] = $link . '/' . $entry;
                    }
                }
                closedir($handle);
            }
        }

        array_multisort($imagens);
        $imagens_ = array();

        foreach($imagens as $imagem_) {
            $imagens_[] = $imagem_;
        }

        return $imagens_;
    }

    public function bodyEmail($titulo,$corpo,$email,$nomeUsuario) {

        $address = $_SERVER['SERVER_NAME'].base_url();
        
        $html = '
        <!doctype html>
        <html>
          <head>
            <meta name="viewport" content="width=device-width" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>'.$titulo.'</title>
            <style>
              /* ------------------------------------- GLOBAL RESETS ------------------------------------- */ /*All the styling goes here*/ img{border: none; -ms-interpolation-mode: bicubic; max-width: 100%;}body{background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 15; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;}table{border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;}table td{font-family: sans-serif; font-size: 14px; vertical-align: top;}/* ------------------------------------- BODY & CONTAINER ------------------------------------- */ .body{background-color: #f6f6f6; width: 100%;}/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */ .container{display: block; margin: 0 auto !important; /* makes it centered */ max-width: 580px; padding: 10px; width: 580px;}/* This should also be a block element, so that it will fill 100% of the .container */ .content{box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;}/* ------------------------------------- HEADER, FOOTER, MAIN ------------------------------------- */ .main{background: #ffffff; border-radius: 3px; width: 100%;}.wrapper{box-sizing: border-box; padding: 20px;}.content-block{padding-bottom: 10px; padding-top: 10px;}.footer{clear: both; margin-top: 10px; text-align: center; width: 100%;}.footer td, .footer p, .footer span, .footer a{color: #999999; font-size: 12px; text-align: center;}/* ------------------------------------- TYPOGRAPHY ------------------------------------- */ h1, h2, h3, h4{color: #000000; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; margin-bottom: 30px;}h1{font-size: 35px; font-weight: 300; text-align: center; text-transform: capitalize;}p, ul, ol{font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;}p li, ul li, ol li{list-style-position: inside; margin-left: 5px;}a{color: #3498db; text-decoration: underline;}/* ------------------------------------- BUTTONS ------------------------------------- */ .btn{box-sizing: border-box; width: 100%;}.btn > tbody > tr > td{padding-bottom: 15px;}.btn table{width: auto;}.btn table td{background-color: #ffffff; border-radius: 5px; text-align: center;}.btn a{background-color: #ffffff; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; color: #3498db; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize;}.btn-primary table td{background-color: #3498db;}.btn-primary a{background-color: #27ae60; border-color: #27ae60; color: #ffffff;}/*------------------------------------- OTHER STYLES THAT MIGHT BE USEFUL ------------------------------------- */ .last{margin-bottom: 0;}.first{margin-top: 0;}.align-center{text-align: center;}.align-right{text-align: right;}.align-left{text-align: left;}.clear{clear: both;}.mt0{margin-top: 0;}.mb0{margin-bottom: 0;}.preheader{color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;}.powered-by a{text-decoration: none;}hr{border: 0; border-bottom: 1px solid #f6f6f6; margin: 20px 0;}/* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */ table[class=body] h1{font-size: 28px !important; margin-bottom: 10px !important;}table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a{font-size: 16px !important;}table[class=body] .wrapper, table[class=body] .article{padding: 10px !important;}table[class=body] .content{padding: 0 !important;}table[class=body] .container{padding: 0 !important; width: 100% !important;}table[class=body] .main{border-left-width: 0 !important; border-radius: 0 !important; border-right-width: 0 !important;}table[class=body] .btn table{width: 100% !important;}table[class=body] .btn a{width: 100% !important;}table[class=body] .img-responsive{height: auto !important; max-width: 100% !important; width: auto !important;}/* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */ @media all{.ExternalClass{width: 100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;}.apple-link a{color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; text-decoration: none !important;}.btn-primary table td:hover{background-color: #34495e !important;}.btn-primary a:hover{background-color: #27ae40 !important; border-color: #27ae50 !important;}}
            </style>
          </head>
          <body width="100%">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" width="100%">
              <tr>
                <td>&nbsp;</td>
                <td class="container">
                  <div class="content">
                        <tr>
                            <td width="100%" align="center">
                                <img src="http://'.$address.'style/img/icon.png" width="100px" height="100px" style="border-radius: 10px;">
                            </td>
                        </tr>
        
                      <!-- START MAIN CONTENT AREA -->
                      <tr>
                        <td class="wrapper">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td>
                                <p>Olá '.$nomeUsuario.'.</p>
                                '.$corpo.'
                                <p>Esse e-mail é automático, qualquer dúvida entre em contato com nosso suporte <a href="mailto:sacleilao24h@gmail.com">sacleilao24h@gmail.com</a>.</p>
                                <p>Obrigado por utilizar nosso sistema!.</p>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
        
                    <!-- END MAIN CONTENT AREA -->
                    </table>
        
                    <!-- START FOOTER -->
                    <div class="footer">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="content-block" colspan="2">
                            <span class="apple-link"><a href="http://'.$address.'" target="_blank">Leilão 24H</a></span>
                          </td>
                        </tr>
                        <tr>
                          <td class="content-block powered-by" colspan="2">
                            Powered by <a href="http://megamil.net">Megamil.net</a>.
                          </td>
                        </tr>
                        <tr align="left">
                          <td class="content-block powered-by" colspan="2" align="left">
                            <small>Solicitado pelo IP: '.$_SERVER['REMOTE_ADDR'].'</small> <br>
                            <small>Sistema / Navegador: '.$_SERVER['HTTP_USER_AGENT'].'</small> <br>
                            <small>Em: '.date('d/m/Y H:i:s').'</small> <br>
                            <br>
                            <a href="http://'.$address.'privacidade">Política de privacidade</a> <br> 
                            <a href="http://'.$address.'">Termos de uso</a>
                          </td>
                        </tr>
        
                      </table>
                    </div>
                    <!-- END FOOTER -->
        
                  <!-- END CENTERED WHITE CONTAINER -->
                  </div>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </body>
        </html>';

        // Detalhes do Email. 
        $this->email->from('no-reply@leilao24h.com.br', 'Leilão 24H')
                    ->to($email)
                    ->subject($titulo)
                    ->message($html);

        // Enviar... 
        if ($this->email->send()) {

            return true;

        } else {

            log_message('error', 'Erro e-mail '.$this->email->print_debugger());

            return false;

        }
       
    }

}