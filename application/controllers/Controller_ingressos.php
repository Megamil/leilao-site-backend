<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_ingressos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->load->model('model_ingressos');
		$this->load->model('model_usuarios');
	}

	public function pagamento() {
		$this->load->view('pagamento/pagamento');
	}

	public function comprar_pacote() {

		$id_usuario = $this->uri->segment(2); //id_usuario
		$id_pacote = $this->uri->segment(3); //id_pacote

		$usuario = $this->model_usuarios->get(array('id_usuario' => $id_usuario));
		$pacote = $this->model_ingressos->getPacote($id_pacote);

		$dados = array(
			'usuario' => $usuario,
			'pacote' => $pacote
		);

		$this->load->view('ingressos/view_comprar_ingressos', $dados);
	}

	public function pagar_pacote() {
		
		$id_usuario = $this->input->post('id_usuario');
		$id_pacote_ingresso = $this->input->post('id_pacote');

		$pacote = $this->model_ingressos->getPacote($id_pacote_ingresso);

		$cad_ingressos = array(
			'fk_usuario' => $id_usuario,
			'quantidade' => $pacote['quantidade_ingresso'],
			'gratis' => $pacote['gratis']
		);

		$compra_ingresso = array(
			'valor' => $pacote['valor_pacote'],
			'fk_pacote' => $pacote['id_pacote_ingresso'],
		);

		$this->model_ingressos->start();

		$id_ingresso = $this->model_ingressos->insertIngresso($cad_ingressos);

		$compra_ingresso['fk_ingresso'] = $id_ingresso;

		$id_comprar = $this->model_ingressos->insertCompra($compra_ingresso);

		$update = array(
			'id_ingresso' => $id_ingresso,
			'ativo' => true
		);

		$this->model_ingressos->updateIngresso($update);

		$commit = $this->model_ingressos->commit();

		if ($commit['status']) {
			$this->redirecionar(false,"credito inserido, sem paypal");
		} else {
			$this->redirecionar(true, "Falha na compra");
		}
	}

	function redirecionar($erro = false, $mensagem = '') {

		$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
		$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
		$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

		if($iPod || $iPhone || $iPad){

		    echo '<center> <img src="'.base_url().'style/img/favicon.ico" style="width: 300px;margin-top:30px"></center>
		        <p style="text-align: center; font-size: 40px;"> '.$mensagem.'</p>
		        <script type="text/javascript">
		            alert("'.$mensagem.'");
		            setInterval(function () {
		                window.location = \'megamil.net.webview-avancado://voltar\';
		            }, 5000);
		        </script>';

		} else if($Android){

		    echo '<center> <img src="'.base_url().'style/img/favicon.ico" style="width: 300px;margin-top: 30px"></center>
		        <p style="text-align: center; font-size: 40px;"> '.$mensagem.'</p>
		        <script type="text/javascript">
		            Android.exibirAvisoAndroid(\''.$mensagem.'\');
		        </script>';

		} else { //Caso abra pelo Navegador de MAC / PC / Linux

		   $this->aviso('Erro','Problemas ao executar o pagamento. '.$mensagem,'success',true);

		    $server = $_SERVER['SERVER_NAME'];
		    header('Location: http://'.$server.base_url());

		}
	}
}

/* End of file Controller_ingressos.php */
/* Location: ./application/controllers/Controller_ingressos.php */