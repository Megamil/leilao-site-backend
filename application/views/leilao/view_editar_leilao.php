<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Editar Leilão</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php echo form_open('Controller_leilao/editar_leilao'); ?>

<div class="row">

	<input type="hidden" name="id_leilao" value="<?php echo $this->session->flashdata('id_leilao_edicao'); ?>">
    
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="fk_produto">Produto</label> 
				<select class="form-control obrigatorio" id="fk_produto" name="fk_produto" aviso="Produto" readonly="readonly">
				<?php 

					foreach ($dados_iniciais as $produto) {
						if($this->session->flashdata('fk_produto_edicao') == $produto->id_produto){
							echo '<option value="'.$produto->id_produto.'">'.$produto->nome_produto.'</option>';
						}						
					}

				 ?>
				</select>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="valor_minimo">Valor Mínimo Para Cenda</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control mascara_monetaria obrigatorio" id="valor_minimo" name="valor_minimo" placeholder="Valor Mínimo" aviso="Valor Mínimo" value="<?php echo $this->session->flashdata('valor_minimo_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="lance_minimo">Lance Mínimo</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control mascara_monetaria obrigatorio validar_numeros" id="lance_minimo" name="lance_minimo" placeholder="Lance Mínimo" aviso="Lance Mínimo" value="<?php echo $this->session->flashdata('lance_minimo_edicao'); ?>">
		</div>
    </div>

    <div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="ingressos">Ingressos</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_numeros" id="ingressos" name="ingressos" placeholder="Ingresso" aviso="Ingresso" value="<?php echo $this->session->flashdata('ingressos_edicao'); ?>">
		</div>
	</div>

</div>
<div class="row">

    <div class="col-md-6">
		<div class="form-group has-feedback">
			<label class="control-label" for="periodo">Período</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio range_timestamp" id="periodo" name="periodo" placeholder="Data Início" aviso="Data Início" value="<?php echo $this->session->flashdata('periodo_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="quantidade_minima_usuarios">Qtd de Usuários p/ Iniciar</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="quantidade_minima_usuarios" name="quantidade_minima_usuarios" placeholder="Quantidade Mínimas de usuários para começar" aviso="Quantidade Mínimas de usuários para começar" value="<?php echo $this->session->flashdata('quantidade_minima_usuarios_edicao'); ?>" >
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="tempo_iniciar">Cronometro (Minutos)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="tempo_iniciar" name="tempo_iniciar" placeholder="Cronometro Em (Minutos)" aviso="Cronometro Em (Minutos)" value="<?php echo $this->session->flashdata('tempo_iniciar_edicao'); ?>" >
		</div>
	</div>

</div>

<div class="row">

    <div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="valor_inicial_lance">Valor inicial para lances</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control mascara_monetaria obrigatorio" id="valor_inicial_lance" name="valor_inicial_lance" placeholder="Valor inicial para lances" aviso="Valor inicial para lances" value="<?php echo $this->session->flashdata('valor_inicial_lance_edicao'); ?>" <?php if($this->session->flashdata('ingressos_edicao') > 0) { echo "readonly"; } ?>>
		</div>
	</div>

</div>

<script>
	$(document).ready(function(){
		$("#ingressos").change(function(){
			if($(this).val() > 0){
				$("#valor_inicial_lance").val($("#valor_minimo").val());
				$("#valor_inicial_lance").prop("readonly",true);
			} else {
				$("#valor_inicial_lance").val($("#valor_minimo").val());
				$("#valor_inicial_lance").prop("readonly",false);
			}
		});

		$("#valor_minimo").change(function(){
			if($("#ingressos").val() > 0){
				$("#valor_inicial_lance").val($("#valor_minimo").val());
			}
		});

	});
</script>

<h3>Para Calcular o Frete</h3>
<hr>
<div class="row">
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="altura">Altura (cm)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_decimais" id="altura" name="altura" placeholder="Altura" aviso="Altura" value="<?php echo $this->session->flashdata('altura_edicao'); ?>" >
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="largura">Largura (cm)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_decimais" id="largura" name="largura" placeholder="Largura" aviso="Largura" value="<?php echo $this->session->flashdata('largura_edicao'); ?>" >
		</div>
	</div>	
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="comprimento">Comprimento (cm)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_decimais" id="comprimento" name="comprimento" placeholder="Comprimento" aviso="Comprimento" value="<?php echo $this->session->flashdata('comprimento_edicao'); ?>" >
		</div>
	</div>	
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="peso">Peso (kg)</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_decimais" id="peso" name="peso" placeholder="Peso" aviso="Peso" value="<?php echo $this->session->flashdata('peso_edicao'); ?>" >
		</div>
	</div>						
</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Editar"> <i class="glyphicon glyphicon-floppy-disk"></i> Editar </button>
	</div>
</div>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){

		$('#fk_produto').val(<?php echo $this->session->flashdata('fk_produto_edicao'); ?>).trigger('change');
		
	});
</script>
