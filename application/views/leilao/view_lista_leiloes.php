<div class="row" >
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-gift"></i> Lista de leilões</h3>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/20">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Leilão
		</a>
	</div>
</div>
<table class="table table-bordered table-hover" align="center">

    <thead>
        <tr>
            <th>Categoria</th>
            <th>Criado por</th>
            <th>Produto</th>
            <th>Valor Mínimo</th>
            <th>Lance Mínimo</th>
            <th>Valor Atual</th>
            <th>Ingressos</th>
            <th>Pessoas Na Sala</th>
            <th>Ínicio</th>
            <th>Fim</th>
            <th>Status</th>
            <th>Dados de quem arrematou</th>
        </tr>
    </thead>
    <tbody>
<?php

    foreach($dados_iniciais['leiloes'] as $leilao){
        echo "<tr>";
		echo "<td>{$leilao->nome_categoria}</td>";
        echo "<td>{$leilao->nome_usuario_leiloando}</td>";
		echo "<td>{$leilao->nome_produto}</td>";
		echo "<td class=\"mascara_monetaria\">{$leilao->valor_minimo}</td>";
		echo "<td class=\"mascara_monetaria\">{$leilao->lance_minimo}</td>";
		echo "<td class=\"mascara_monetaria\">{$leilao->lance_formatado}</td>";
		echo "<td>{$leilao->ingressos}</td>";
        echo "<td>{$leilao->usuarios}</td>";
		echo "<td>{$leilao->data_inicio}</td>";
		echo "<td>{$leilao->data_fim_previsto}</td>";
		echo "<td>{$leilao->status}</td>";
        if(is_null($leilao->fk_usuario_arrematou)){
            echo '<td>Não Arrematado</td>';
        } else {
            echo '<td><button cod="'.$leilao->fk_usuario_arrematou.'" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"> <i class="glyphicon glyphicon-edit"> </i> Detalhes</button></td>';
        }
        echo "</tr>";
    }


?>
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 align="center" class="modal-title" id="myModalLabel"><strong class="titulo_modal">Detalhes</strong></h3>
      </div>
      <div class="modal-body">
        <div id="detalhes"></div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    $('#myModal').on('show.bs.modal', function (event) {

        var id = $(event.relatedTarget).attr('cod');
        $('#detalhes').load('<?php echo base_url() ?>Controller_usuarios/detalhes_usuario',{id: id}, function(){});

        $(document).on('click','#inativar_usuario',function(){

            $.post('<?php echo base_url() ?>Controller_usuarios/status_usuario', { id: id, ativar: 0 }, function(){location.reload();});

        });

        $(document).on('click','#ativar_usuario',function(){

            $.post('<?php echo base_url() ?>Controller_usuarios/status_usuario', { id: id, ativar: 1 }, function(){location.reload();});

        });

    })
</script>