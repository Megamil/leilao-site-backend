<!-- View Categorias -->
<div class="main_categorias">

	<div class="container-fluid">

			<div class="categorias">

				<!-- Open Panel -->
				<div class="panel panel-default">

					<!-- Open header panel -->
				  <div class="panel-heading">
						<h1>Categorias</h1>
					</div>
					<!-- Close header panel -->

					<!-- Corpo do panel -->
				  <div class="panel-body">

						<div class="row">

							<div class="col-md-12">
								<h3>Cadastrar</h3>
							</div>

							<div class="col-md-12">

								<!-- Open Form Create Category -->
								<?= form_open('Controller_categorias/criar_categoria'); ?>

								<div class="row">

									<!-- Open input name category -->

									<div class="col-md-5">
										<div class="form-group has-feedback">
						            <label class="control-label">Existe categoria acima?</label>
						            <select name="fk_categoria_pai" id="fk_categoria_pai" class="form-control">
						            	<option value="0">Selecione (Opcional)</option>
						            	<?php foreach($dados_iniciais as $categoria){

						            		echo "<option value=\"{$categoria['id_categoria']}\">{$categoria['nome_categoria']}</option>";

						            	} ?>
						            </select>
						        	</div>
									</div>

									<div class="col-md-5">
										<div class="form-group has-feedback">
						            <label class="control-label"><small>*</small>Nome da Categoria</label>
						            <input type="text" class="form-control obrigatorio" id="nome_categoria" name="nome_categoria" placeholder="Nome da Categoria" aviso="Complemento" value="<?= $this->session->flashdata('complemento_usuario'); ?>">
						        	</div>
									</div>
									<!-- Close input name category -->

	    						<!-- Open select status category -->
									<!-- <div class="col-md-5">

										<div class="form-group has-feedback">
											<label class="control-label"><small>*</small>Status da Categoria</label>
											<select class="form-control" name="ativa">
												<option value="">Status da Categoria</option>
												<option value="0">Ativo</option>
												<option value="1">Inativo</option>
											</select>
										</div>

									</div> -->
									<!-- Close select status category -->

									<div class="col-md-2">
										<input type="submit" class="btn btn-success" id="validar_Enviar" name="" value="Cadastrar" title="Cadastrar" alt="[Cadastrar]">
									</div>

								</div>

								<?= form_close(); ?>
								<!-- Close Form Create Category -->

							</div>

							<!-- Listar Categorias -->
							<div class="col-md-12">
								<p class="j_list">Lista de Categorias <i class="glyphicon glyphicon-menu-down"></i> </p>
							</div>

							<div class="col-md-12 list_category" style="display: none;">

								<div class="table-responsive">

									<table class="table table-action">
										<thead>
											<tr>
												<th class="t-medium">Cod</th>
												<th class="t-medium">Nome da Categoria</th>
												<th class="t-medium">Categoria Pai</th>
											</tr>
										</thead>

										<tbody>

											<?php

												foreach($dados_iniciais as $categoria):
													extract($categoria);
											?>

											<tr>
												<td class=""><?= $id_categoria; ?></td>
												<td class=""><?= $nome_categoria; ?></td>
												<td class=""><?= $categoria_pai; ?></td>
											</tr>

										<?php endforeach; ?>

										</tbody>
									</table>

								</div>

							</div>

						</div>

					</div>
					<!-- Close Corpo do panel -->

				</div>
				<!-- Close Panel -->

			</div>

	</div>

</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.j_list').click();
  });
</script>