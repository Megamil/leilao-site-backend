<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="refresh" content="600">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php

    if (isset($titulo_aplicacao)){

        echo '<title>'.PROJETO.' | '.$titulo_aplicacao.'</title>';

     } else {

        echo '<title>'.PROJETO.'</title>';
     }

  ?>
  <link rel="shortcut icon" href="<?= base_url('style/img/favicon.ico'); ?>" />
  <link rel="stylesheet" href="<?= base_url('style/css/boot.css'); ?>" />
	<link rel="stylesheet" href="<?= base_url('style/css/bootstrap.min.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('style/animate/animate.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('style/j-pages/css/jPages.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('style/css/select2.css'); ?>" />
	<link rel="stylesheet" href="<?= base_url('style/css/jquery.toast.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('style/css/estilo.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('style/css/plugin_tinymce.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('style/jquery-ui/jquery-ui.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('style/css/sweetalert.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('style/css/daterangepicker.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('style/css/datatable.css'); ?>" />
  <script src="<?= base_url('style/js/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('style/jquery-ui/jquery-ui.js'); ?>"></script>
  <script src="<?= base_url('style/js/bootstrap.min.js'); ?>"></script>
  <script src="<?= base_url('style/js/jquery.mask.js'); ?>"></script>
  <script src="<?= base_url('style/js/jquery.toast.js'); ?>"></script>
  <script src="<?= base_url('style/js/sweetalert.js'); ?>"></script>
  <script src="<?= base_url('style/js/select2.js'); ?>"></script>
  <script src="<?= base_url('style/js/moment.js'); ?>"></script>
  <script src="<?= base_url('style/js/daterangepicker.js'); ?>"></script>
  <script src="<?= base_url('style/js/jquery.priceformat.js'); ?>"></script>
  <script src="<?= base_url('style/tinymce/tinymce.min.js'); ?>"></script>
  <script src="<?= base_url('style/js/plugin_tinymce.js'); ?>"></script>
  <script src="<?= base_url('style/j-pages/js/jPages.js'); ?>"></script>
  <script src="<?= base_url('style/js/script.js'); ?>"></script>

    <!-- DataTable -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>style/DataTables/datatables.min.css"/>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/DataTables/DataTables/js/jquery.dataTables.js"></script> 
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/datatable.js"></script>

</head>
<body>

<?php if (isset($menu)) { ?>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= base_url(); ?>"><img src="<?= base_url(); ?>style/img/favicon.ico" width="35px" height="30px;"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <?= $menu; ?>
      </ul>
    </div>
    <!-- /.navbar-collapse -->

  </div>
  <!-- /.container-fluid -->
</nav>

<?php } ?>

<!--Div Principal-->
<div class="container principal" hidden="hidden">

<div class="modal fade" id="modal_historico" tabindex="-1" role="dialog" aria-labelledby="modal_historicoLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_historicoLabel">Histórico de edições</h4>
      </div>
      <div class="modal-body">

        <div class="progress_modal_historico">
          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
            <span class="sr-only">50%</span>
          </div>
        </div>

        <div id="load_modal_historico"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
