<div class="row" >
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-gift"></i> Lista de recompensas</h3>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/22">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Produto
		</a>
	</div>
</div>
<table class="table table-bordered table-hover" align="center">

    <thead>
        <tr>
            <th>Entregue</th>
            <th>Solicitou Em:</th>
            <th>Usuário</th>
            <th>Produto</th>
            <th class="no-filter">Ver Cliente</th>
            <th class="no-filter">Ver Produto</th>
        </tr>
    </thead>
    <tbody>

<?php

    foreach($dados_iniciais as $recompensa){
        echo "<tr>";
         echo '<td><button cod="'.$recompensa->id_compra_recompensa.'" class="btn btn-success entrega"> <i class="glyphicon glyphicon-edit"> </i> Entregue</button></td>';
        echo "<td>{$recompensa->data_compra}</td>";
        echo "<td>{$recompensa->nome_usuario} E-mail: {$recompensa->email_usuario} Tel: <span class=\"mascara_cel\">{$recompensa->celular_usuario}</span></td>";
        echo "<td>{$recompensa->nome_produto}</td>";
        echo '<td><button cod="'.$recompensa->id_usuario.'" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> <i class="glyphicon glyphicon-eye-open"> </i> Cliente</button></td>';
        echo '<td><a href="'.base_url().'main/redirecionar/23/'.$recompensa->id_produto.'" target="_blank"><button class="btn btn-info"> <i class="glyphicon glyphicon-eye-open"> </i> Produto</button></a></td>';
        echo "</tr>";
    }

?>

    </tbody>
</table>

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 align="center" class="modal-title" id="myModalLabel"><strong class="titulo_modal">Detalhes</strong></h3>
      </div>
      <div class="modal-body">
        <div id="detalhes"></div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

    $(document).ready(function(){

        $('.entrega').click(function(){

            var cod = $(this).attr('cod');

            swal({
              title: "Confirmar entrega?",
              text: 'Confirma a entrega do produto? essa ação é irreversível!',
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              cancelButtonText: "Não",
              confirmButtonText: "Sim, Produto entregue",
              closeOnConfirm: false
            },function(){
                
                $.post('<?php echo base_url() ?>Controller_produtos/entrega', { cod: cod }, function(){location.reload();});
                
                location.reload();
            });

        });

    });

    $('#myModal').on('show.bs.modal', function (event) {

        var id = $(event.relatedTarget).attr('cod');
        $('#detalhes').load('<?php echo base_url() ?>Controller_usuarios/detalhes_usuario',{id: id}, function(){});

        $(document).on('click','#inativar_usuario',function(){

            $.post('<?php echo base_url() ?>Controller_usuarios/status_usuario', { id: id, ativar: 0 }, function(){location.reload();});

        });

        $(document).on('click','#ativar_usuario',function(){

            $.post('<?php echo base_url() ?>Controller_usuarios/status_usuario', { id: id, ativar: 1 }, function(){location.reload();});

        });

    })
</script>