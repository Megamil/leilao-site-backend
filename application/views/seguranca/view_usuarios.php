<hr>
<div class="row">
	<div class="col-md-8">
		<h3> <i class="glyphicon glyphicon-stats"></i> Relatório Usuários</h3>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/7">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Usuário
		</a>
	</div>
</div>

<table class="table table-bordered table-hover" align="center">

    <thead>
        <tr>
            <th class="no-filter">Ver</th>
            <th>ID</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Login</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
<?php

    foreach($dados_iniciais['resultado'] as $usuario){
			if($usuario->id != 1 && $usuario->id != $this->session->userdata('usuario')){
				echo "<tr>";
				echo '<td><a href="'.base_url().'main/redirecionar/5/'.$usuario->id.'"><button class="btn btn-info"> <i class="glyphicon glyphicon-edit"> </i> Editar</button></a></td>';
        echo "<td>{$usuario->id}</td>";
        echo "<td>{$usuario->nome_usuario}</td>";
        echo "<td>{$usuario->email_usuario}</td>";
        echo "<td>{$usuario->login_usuario}</td>";
        echo "<td>{$usuario->ativo}</td>";
				echo "</tr>";
			}
    }


?>
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 align="center" class="modal-title" id="myModalLabel"><strong class="titulo_modal">Detalhes</strong></h3>
      </div>
      <div class="modal-body">
        <div id="detalhes"></div>
      </div>
    </div>
  </div>
</div>