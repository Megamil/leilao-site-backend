<script type="text/javascript">
	history.replaceState({pagina: "lista_usuarios"}, "Lista dos usuários ", "<?php echo base_url() ?>main/redirecionar/2");
</script>

<div class="row">
	<div class="col-md-6">
		<h1> <i class="glyphicon glyphicon-pencil"></i> Atualizar Perfil</h1>
	</div>
	<div class="col-md-6" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal_historico" id="historico_modal" cod="<?php echo $this->session->flashdata('id_usuario_edicao'); ?>"> <i class="glyphicon glyphicon-time"></i> Histórico Edições</button>
	</div>
</div>
<hr>

<?php echo form_open('controller_usuarios/editar_perfil'); ?>

<!-- Campos ocultos -->
<input type="hidden" name="login_inicial" value="<?php echo $this->session->flashdata('login_usuario_edicao'); ?>">
<input type="hidden" name="email_inicial" value="<?php echo $this->session->flashdata('email_usuario_edicao'); ?>">

<div class="row">

	<div class="col-md-2">
		<div class="form-group has-feedback has-success">
			<label class="control-label" for="id_usuario">ID</label> 
			<i class="glyphicon glyphicon-lock form-control-feedback"></i>
			<input type="text" class="form-control" id="id_usuario" name="id_usuario" placeholder="ID" value="<?php echo $this->session->flashdata('id_usuario_edicao'); ?>" readonly>
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_usuario">Nome do usuário</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="nome_usuario" name="nome_usuario" placeholder="Nome usuário" aviso="Nome usuário" value="<?php echo $this->session->flashdata('nome_usuario_edicao'); ?>"  maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="email_usuario">E-mail</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio validar_email_usuario" id="email_usuario" name="email_usuario" placeholder="E-mail" aviso="E-mail" value="<?php echo $this->session->flashdata('email_usuario_edicao'); ?>"  maxlength="40">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="telefone_usuario">Celular</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio mascara_cel" id="telefone_usuario" name="telefone_usuario" placeholder="Celular" aviso="Celular" value="<?php echo $this->session->flashdata('telefone_usuario_edicao'); ?>">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="login_usuario">Login do usuário</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="login_usuario" name="login_usuario" placeholder="Login do usuário" aviso="Login do usuário" value="<?php echo $this->session->flashdata('login_usuario_edicao'); ?>"  maxlength="20">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="senha_usuario">Alterar Senha</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="password" class="form-control" id="senha_usuario" name="senha_usuario" placeholder="Senha" aviso="Senha">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="confirmacaoSenha">Confirme a nova Senha</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="password" class="form-control" id="confirmacaoSenha" name="confirmacaoSenha" placeholder="Confirme a Senha" aviso="Confirme a Senha">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cep_usuario">CEP</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio mascara_cep" id="cep_usuario" name="cep_usuario" placeholder="CEP" aviso="CEP" value="<?php echo $this->session->flashdata('cep_usuario_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="logradouro_usuario">Logradouro</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="logradouro_usuario" name="logradouro_usuario" placeholder="Logradouro" aviso="Logradouro" value="<?php echo $this->session->flashdata('logradouro_usuario_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="bairro_usuario">Bairro</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="bairro_usuario" name="bairro_usuario" placeholder="Bairro" aviso="Bairro" value="<?php echo $this->session->flashdata('bairro_usuario_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cidade_usuario">Cidade</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="cidade_usuario" name="cidade_usuario" placeholder="Cidade" aviso="Cidade" value="<?php echo $this->session->flashdata('cidade_usuario_edicao'); ?>">
		</div>
	</div>

</div>

<div class="row">
	
	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="estado_usuario">Estado</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<select class="form-control obrigatorio" name="estado_usuario" id="estado_usuario">
				<option value="1">AC</option>
				<option value="2">AL</option>
				<option value="3">AP</option>
				<option value="4">AM</option>
				<option value="5">BA</option>
				<option value="6">CE</option>
				<option value="7">DF</option>
				<option value="8">ES</option>
				<option value="9">GO</option>
				<option value="10">MA</option>
				<option value="11">MT</option>
				<option value="12">MS</option>
				<option value="13">MG</option>
				<option value="14">PA</option>
				<option value="15">PB</option>
				<option value="16">PR</option>
				<option value="17">PE</option>
				<option value="18">PI</option>
				<option value="19">RJ</option>
				<option value="20">RN</option>
				<option value="21">RS</option>
				<option value="22">RO</option>
				<option value="23">RR</option>
				<option value="24">SC</option>
				<option value="25">SP</option>
				<option value="26">SE</option>
				<option value="27">TO</option>
			</select>
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="numero_usuario">Número</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control obrigatorio" id="numero_usuario" name="numero_usuario" placeholder="Número" aviso="Número" value="<?php echo $this->session->flashdata('numero_usuario_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group has-feedback">
			<label class="control-label" for="complemento_usuario">Complemento</label> 
			<i class="glyphicon glyphicon-pencil form-control-feedback"></i>
			<input type="text" class="form-control" id="complemento_usuario" name="complemento_usuario" placeholder="Complemento" aviso="Complemento" value="<?php echo $this->session->flashdata('complemento_usuario_edicao'); ?>">
		</div>
	</div>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Salvar Edição"> <i class="glyphicon glyphicon-floppy-disk"></i> Atualizar Perfil </button>
	</div>
</div>

<?php echo form_close(); ?>


<script type="text/javascript">
	$(document).ready(function(){

		$('#estado_usuario').val(<?php echo $this->session->flashdata('estado_usuario_edicao'); ?>).trigger('change');

		$("#cep_usuario").focusout(function(){

			var settings = {
			"async": true,
			"crossDomain": true,
			"url": "<?php echo base_url(); ?>controller_webservice/buscar_cep?cep="+$("#cep_usuario").val(),
			"method": "GET"
			}

			$.ajax(settings).done(function (response) {
				$("#cep_usuario").val(response.endereco.cep);
				$("#logradouro_usuario").val(response.endereco.logradouro);
				$("#bairro_usuario").val(response.endereco.bairro);
				$("#cidade_usuario").val(response.endereco.localidade);
				$("#complemento_usuario").val(response.endereco.complemento);
				$('#estado_usuario').val(response.endereco.uf_id).trigger('change');
				console.log(response);
			});

		});

	});
</script>