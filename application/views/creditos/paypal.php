<div id="paypal-button"></div>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<script>
    paypal.Button.render({

        env: 'sandbox', // Or 'sandbox'

        client: {
            sandbox:    'AUShjTwjxegVw1hJAUgk0OF3dfY8Eu6ZqJz5kNJ48--0_VgxGbB9xy_JrQ38ZwZAMSqfvYhS2VrmSOkw',
            production: 'xxxxxxxxx'
        },

        commit: true, // Show a 'Pay Now' button

        payment: function(data, actions) {
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: '1.00', currency: 'BRL

                            ,' }
                        }
                    ]
                }
            });
        },

        onAuthorize: function(data, actions) {
            return actions.payment.execute().then(function(payment) {

                // The payment is complete!
                // You can now show a confirmation message to the customer

            });
        }

    }, '#paypal-button');
</script>